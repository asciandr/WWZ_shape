Run batch parallelisation of histogram creation (from ntuples) with TRexFitter
---------
This example shows how to speed up the production of input histogram (from ntuples), necessary to create the RooStats xmls and workspaces (which is much faster). Just check ``job.sh``, make sure that the ``PACKAGE_folder`` and ``TREX_folder`` are matching the folders where you have this package and the TRexFitter package, respectively. Also check that all needed files to run your config are being copied to the pool where the job is going to be run. At this point just run the bash script::

  ./split_fits.sh CONFIG.config

giving your TRex config as input, where the regions are defined in the format ``Region: "MY_REGION"``. Sit back, relax and wait for your histograms to be there, ready to produce the workspace and run!

If you are crazy you can parallelise both region-by-region and sample-by-sample, but keep in mind that some features (e.g. auto-binning or ghost samples) will not work in this case. Feed a fit config file::

  ./complete_split_fits.sh COMPLETE_parallelisation.config

After running all batch jobs, hadd the relevant histogram and smoothly run the options ``wf``.
