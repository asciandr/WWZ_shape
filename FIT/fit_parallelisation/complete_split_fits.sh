#!/bin/bash

listtxt=$*

JOBLINE=`cat $listtxt | grep Job: `
JOBNAME=`echo "$JOBLINE" | cut -d'"' -f 2`

echo "JOBLINE: $JOBLINE"
echo "JOBNAME: $JOBNAME"

while read config
do
    if [[ $config = Region:* ]]; then
        echo ""
        echo "I have found a 'Region'"
        echo "$config"
        SUBSTRING=`echo "$config" | cut -d'"' -f 2`
        echo "The region is called $SUBSTRING"
        echo "Going to run $listtxt to produce histograms related to this region"
        while read config2
        do
            if [[ $config2 = Sample:* ]]; then
                echo ""
                echo "I have found a 'Sample'"
                echo "$config2"
                SUBSTRING2=`echo "$config2" | cut -d'"' -f 2`
                echo "The sample is called $SUBSTRING2"
                echo "Going to run $listtxt to produce histograms related to this sample"
                cat complete_job.sh | sed "s/JOB_NAME/$JOBNAME/g" | sed "s/SAMPLE_NAME/$SUBSTRING2/g" | sed "s/REGION_NAME/$SUBSTRING/g" | sed "s/CONFIG/$listtxt/g" > complete_job_${SUBSTRING}_${SUBSTRING2}.sh
                chmod 755 complete_job_${SUBSTRING}_${SUBSTRING2}.sh
                #queue="8nh"
                queue="1nd"
                bsub -q $queue complete_job_${SUBSTRING}_${SUBSTRING2}.sh
            fi
        done < $*
    fi
done < $*
