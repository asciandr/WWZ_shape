# WVZ fit
# Andrea Sciandra 
# andrea.sciandra@cern.ch

#--------------- # 
#---  JOB    --- # 
#--------------- # 

Job: "three_leptons_fit"
  CmeLabel: "13 TeV"
  POI: "mu_XS_WVZ"
  ReadFrom: NTUP
  NtuplePaths: "/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/3L_BDT_output/"
  LumiLabel: "80.0 fb^{-1}"
  Lumi: 79888.3
  MCweight: "(mcWeightOrg*xsec*(1./tot_weight)*pileupEventWeight*JVT_EventWeight)"
  PlotOptions: "YIELDS","NORMSIG"
#  PlotOptions: "YIELDS"
  NtupleName: "nominal"
  DebugLevel: 9
  BlindingThreshold: 0.1
  SystControlPlots: TRUE
  SystErrorBars: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  SystLarge: 1
  CorrelationThreshold: 0.05
  RankingMaxNP: 30
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE
  MCstatThreshold: 0.01
  ImageFormat: "png"
  Selection: "1"
  ReplacementFile: my_Common_XS_unc.txt
  StatOnly: TRUE

# --------------- # 
# ---  FIT    --- # 
# --------------- # 

Fit: "fit"
  UseMinos: "all"
  UseMinos: "mu_XS_WVZ"
#  FitType: BONLY 
  FitType: SPLUSB
  FitRegion: CRSR
  POIAsimov: 1
  FitBlind: TRUE
  doLHscan: "mu_XS_WVZ"
  StatOnlyFit: TRUE

# --------------- # 
# ---  LIMIT  --- # 
# --------------- # 

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  POIAsimov: 1 

# --------------- # 
# --- REGIONS --- # 
# --------------- # 

###--- 3L preselection ---###

Region: "three_lep_presel"
  Type: SIGNAL
  DataType: ASIMOV
  Variable: "BDT0",10,-1.,1.
  Binning: "AutoBin","TransfoD",5.,7.
  VariableTitle: "BDT response"
  Label: "WVZ-3l-PreSel"
  ShortLabel: "WVZ-3l-PreSel"
  Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==3) && (abs(total_charge)==1) && (lepton_PromptLeptonVeto[0]<-0.4 && lepton_isTightLH[0] && lepton_PromptLeptonVeto[1]<-0.4 && lepton_isTightLH[1] && lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3)) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3)) && (nJets_mv2c10_70==0) && (@jet_pt.size()>0)  )"

# --------------- # 
# --- SAMPLES --- # 
# --------------- # 

Sample: "WVZ"
  Type: SIGNAL 
  Title: "WVZ"
  NtupleFiles: "WVZ"
  FillColor: 2
  LineColor: 1

Sample: "ZZ"
  Type: BACKGROUND
  Title: "ZZ"
  NtupleFiles: "ZZ"
  FillColor: 428
  LineColor: 1

Sample: "WZ"
  Type: BACKGROUND
  Title: "WZ"
  NtupleFiles: "WZ"
  FillColor: 46
  LineColor: 1

Sample: "lllljj"
  Type: BACKGROUND
  Title: "lllljj"
  NtupleFiles: "lllljj"
  FillColor: 4
  LineColor: 1

Sample: "lllvjj"
  Type: BACKGROUND
  Title: "lllvjj"
  NtupleFiles: "lllvjj"
  FillColor: 3
  LineColor: 1

Sample: "Zjets"
  Type: BACKGROUND
  Title: "Zjets"
  NtupleFiles: "Zjets"
  FillColor: 11
  LineColor: 1

Sample: "Zgamma"
  Type: BACKGROUND
  Title: "Zgamma"
  NtupleFiles: "Zgamma"
  FillColor: 12
  LineColor: 1

Sample: "ttZ"
  Type: BACKGROUND
  Title: "ttZ"
  NtupleFiles: "ttZ"
  FillColor: 13
  LineColor: 1

Sample: "tZ"
  Type: BACKGROUND
  Title: "tZ"
  NtupleFiles: "tZ"
  FillColor: 14
  LineColor: 1

Sample: "tWZ"
  Type: BACKGROUND
  Title: "tWZ"
  NtupleFiles: "tWZ"
  FillColor: 15
  LineColor: 1

Sample: "VH"
  Type: BACKGROUND
  Title: "VH"
  NtupleFiles: "VH"
  FillColor: 5
  LineColor: 1

Sample: "others"
  Type: BACKGROUND
  Title: "others"
  NtupleFiles: "others"
  FillColor: 16
  LineColor: 1

#
#Sample: "data"
#  Type: DATA
#  Title: "data"
#  NtupleFiles: "data"

# ----------------------------- # 
# --- Parameter of Interest --- # 
# ----------------------------- # 

NormFactor: "mu_XS_WVZ"
  Title: "mu_XS(WWZ)"
  Nominal: 1
  Min: -10
  Max: 200
  Samples: "WVZ"

#NormFactor: "N_ZZ"
#  Title: "N(ZZ)"
#  Nominal: 1
#  Min: -10
#  Max: 200
#  Samples: "ZZ","lllljj"

# ------------------- # 
# --- SYSTEMATICS --- # 
# ------------------- # 

###### LUMINOSITY ###### 

#Systematic: "ATLAS_lumi"
#  Title: "ATLAS_lumi"
#  Type: OVERALL
#  OverallUp: 0.032
#  OverallDown: -0.032
#  Category: Instrumental

######### THEORY #########

### VVV ###

###Systematic: "VVV_XS"
###  Title: "VVV_XS"
###  Type: OVERALL
###  OverallUp: 0.50
###  OverallDown: -0.50
###  Samples: WWZ,WZZ,ZZZ 
###  Category: Theory

### tWZ ###

#Systematic: "tWZ_XS"
#  Title: "tWZ_XS"
#  Type: OVERALL
#  OverallUp: XXX_WtZ_XS_UP
#  OverallDown: XXX_WtZ_XS_DO
#  Samples: "tWZ"
#  Category: Theory
#
#### ttH ### 
#
#Systematic: "ttH_XS_QCDscale"
#  Title: "ttH_XS_QCDscale"
#  Type: OVERALL
#  OverallUp: XXX_ttH_XS_QCDscale_UP
#  OverallDown: XXX_ttH_XS_QCDscale_DO
#  Samples: "ttH"
#  Category: Theory
#
#Systematic: "ttH_XS_PDFunc"
#  Title: "ttH_XS_PDFunc"
#  Type: OVERALL
#  OverallUp: XXX_ttH_XS_PDFunc_UP
#  OverallDown: XXX_ttH_XS_PDFunc_DO
#  Samples: "ttH"
#  Category: Theory
#
#### ttZ ###
#
#Systematic: "ttZ_XS_QCDscale"
#  Title: "ttZ_XS_QCDscale"
#  Type: OVERALL
#  OverallUp: XXX_ttZ_XS_QCDscale_UP
#  OverallDown: XXX_ttZ_XS_QCDscale_DO
#  Samples: "ttZ"
#  Category: Theory
#
#Systematic: "ttZ_XS_PDFunc"
#  Title: "ttZ_XS_PDFunc"
#  Type: OVERALL
#  OverallUp: XXX_ttZ_XS_PDFunc_UP
#  OverallDown: XXX_ttZ_XS_PDFunc_DO
#  Samples: "ttZ"
#  Category: Theory

### VV ###

#--- floating VV!!! ---#
#Systematic: "Diboson_XS"
#  Title: "Diboson_XS"
#  Type: OVERALL
##  OverallUp: XXX_diboson_XS_UP
##  OverallDown: XXX_diboson_XS_DO
#  OverallUp: 0.30 
#  OverallDown: -0.30 
#  Samples: VV
#  Category: Theory


######### TTH SCALE #########

#Systematic: ttH_varR
#  Title: ttH_varR
#  WeightSufUp: scale_varRup/scale_nom
#  WeightSufDown: scale_varRdown/scale_nom
#  Smoothing: 40
#  Samples: ttH
#  Category: Theory
#
#Systematic: ttH_varF
#  Title: ttH_varF
#  WeightSufUp: scale_varFup/scale_nom
#  WeightSufDown: scale_varFdown/scale_nom
#  Smoothing: 40
#  Samples: ttH
#  Category: Theory
#
#Systematic: ttH_varRF
#  Title: ttH_varRF
#  WeightSufUp: scale_varRFup/scale_nom
#  WeightSufDown: scale_varRFdown/scale_nom
#  Smoothing: 40
#  Samples: ttH
#  Category: Theory

######### TTZ SCALE #########

#Systematic: ttZ_varR
#  Title: ttZ_varR
#  WeightSufUp: scale_varRup/scale_nom
#  WeightSufDown: scale_varRdown/scale_nom
#  Smoothing: 40
#  Samples: ttZ
#  Category: Theory
#
#Systematic: ttZ_varF
#  Title: ttZ_varF
#  WeightSufUp: scale_varFup/scale_nom
#  WeightSufDown: scale_varFdown/scale_nom
#  Smoothing: 40
#  Samples: ttZ
#  Category: Theory
#
#Systematic: ttZ_varRF
#  Title: ttZ_varRF
#  WeightSufUp: scale_varRFup/scale_nom
#  WeightSufDown: scale_varRFdown/scale_nom
#  Smoothing: 40
#  Samples: ttZ
#  Category: Theory
