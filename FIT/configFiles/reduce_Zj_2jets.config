# WVZ fit
# Andrea Sciandra 
# andrea.sciandra@cern.ch

#--------------- # 
#---  JOB    --- # 
#--------------- # 

Job: "REDUCE_Zjets_combination"
#Job: "combination"
#Job: "SMALL_Bs_and_LEPTON_SFs_impact"
  CmeLabel: "13 TeV"
  POI: "mu_XS_WVZ"
  ReadFrom: NTUP
  NtuplePaths: "/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/v3.0_4L_and_3L_six_BDTs_output/"
  LumiLabel: "80.0 fb^{-1}"
  Lumi: 79888.3
  MCweight: "(mcWeightOrg*xsec*(1./tot_weight)*pileupEventWeight*JVT_EventWeight)"
  PlotOptions: "YIELDS","NORMSIG"
#  PlotOptions: "YIELDS"
  NtupleName: "nominal"
  DebugLevel: 9
  BlindingThreshold: 0.1
  SystControlPlots: TRUE
  SystErrorBars: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  SystLarge: 1
  CorrelationThreshold: 0.0001
  RankingMaxNP: 30
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE
### For the time being exclude gammas 
  MCstatThreshold: 0.80
#  MCstatThreshold: 0.01
  ImageFormat: "png"
  Selection: "1"
  ReplacementFile: my_Common_XS_unc.txt
#  StatOnly: TRUE

# --------------- # 
# ---  FIT    --- # 
# --------------- # 

Fit: "fit"
  UseMinos: "all"
#  UseMinos: "mu_XS_WVZ"
#  FitType: BONLY 
  FitType: SPLUSB
  FitRegion: CRSR
  POIAsimov: 1
  FitBlind: TRUE
  doLHscan: "mu_XS_WVZ"
#  StatOnlyFit: TRUE

# --------------- # 
# ---  LIMIT  --- # 
# --------------- # 

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  POIAsimov: 1 

# ------------------ # 
# --- 4L REGIONS --- # 
# ------------------ # 

###--- WWZ DF SR ---###

### Region: "DF_WWZ_SR"
###   Type: SIGNAL
###   DataType: ASIMOV
### #  Variable: "1",1,0,1.
### #  VariableTitle: "event count"
###   Variable: "BDT2",4,-1.,1.
### #  Binning: "AutoBin","TransfoD",5.,7.
###   VariableTitle: "BDT response"
###   Label: "WWZ-DF-4l-SR"
###   ShortLabel: "WWZ-DF-SR"
###   Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==4) && (total_charge==0) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[0]!=-lepton_ID[3] || Mll03>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3) && (lepton_ID[1]!=-lepton_ID[3] || Mll13>12e3) && (lepton_ID[2]!=-lepton_ID[3] || Mll23>12e3)) && (lepton_pt[3]/1000.>10.) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3 && !(lepton_ID[2]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3 && !(lepton_ID[1]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[3] && abs(Mll03-91.2e3)<10e3 && !(lepton_ID[1]==-lepton_ID[2])) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3 && !(lepton_ID[0]==-lepton_ID[3])) || (lepton_ID[1]==-lepton_ID[3] && abs(Mll13-91.2e3)<10e3 && !(lepton_ID[0]==-lepton_ID[2])) || (lepton_ID[2]==-lepton_ID[3] && abs(Mll23-91.2e3)<10e3 && !(lepton_ID[0]==-lepton_ID[1]))) && (nJets_mv2c10_77==0) && (lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && (lepton_PromptLeptonVeto[3]<-0.4 && lepton_isTightLH[3])  )"
###   MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3)"
### 
### Region: "SF_onShell_SR"
###   Type: SIGNAL 
### #  DataType: DATA
###   DataType: ASIMOV
### #  Variable: "1",1,0,1.
### #  VariableTitle: "event count"
###   Variable: "BDT0",10,-1.,1.
###   Binning: "AutoBin","TransfoD",5.,7.
### #  Binning: "AutoBin","TransfoJ",5.,1.2,5.
### #  Binning: "AutoBin","TransfoF",5.,10.
### #  Binning: -1.,0.8,1.
###   VariableTitle: "BDT response"
###   Label: "SF-onShell-4l-ZR"
###   ShortLabel: "SF-onShell-SR"
###   Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==4) && (total_charge==0) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[0]!=-lepton_ID[3] || Mll03>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3) && (lepton_ID[1]!=-lepton_ID[3] || Mll13>12e3) && (lepton_ID[2]!=-lepton_ID[3] || Mll23>12e3)) && (lepton_pt[3]/1000.>10.) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3 && (lepton_ID[2]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3 && (lepton_ID[1]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[3] && abs(Mll03-91.2e3)<10e3 && (lepton_ID[1]==-lepton_ID[2])) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[3])) || (lepton_ID[1]==-lepton_ID[3] && abs(Mll13-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[2])) || (lepton_ID[2]==-lepton_ID[3] && abs(Mll23-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[1]))) && (nJets_mv2c10_77==0) && (lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && (lepton_PromptLeptonVeto[3]<-0.4 && lepton_isTightLH[3]) && (abs(best_Z_other_Mll-91.2e3)<10e3)  )"
###   MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3)"
### 
### Region: "SF_offShell_SR"
###   Type: SIGNAL 
### #  DataType: DATA
###   DataType: ASIMOV
### #  Variable: "1",1,0,1.
### #  VariableTitle: "event count"
###   Variable: "BDT1",10,-1.,1.
###   Binning: "AutoBin","TransfoD",5.,7.
### #  Binning: "AutoBin","TransfoJ",5.,1.2,5.
### #  Binning: "AutoBin","TransfoF",5.,10.
### #  Binning: -1.,0.8,1.
###   VariableTitle: "BDT response"
###   Label: "SF-offShell-4l-ZR"
###   ShortLabel: "SF-offShell-SR"
###   Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==4) && (total_charge==0) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[0]!=-lepton_ID[3] || Mll03>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3) && (lepton_ID[1]!=-lepton_ID[3] || Mll13>12e3) && (lepton_ID[2]!=-lepton_ID[3] || Mll23>12e3)) && (lepton_pt[3]/1000.>10.) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3 && (lepton_ID[2]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3 && (lepton_ID[1]==-lepton_ID[3])) || (lepton_ID[0]==-lepton_ID[3] && abs(Mll03-91.2e3)<10e3 && (lepton_ID[1]==-lepton_ID[2])) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[3])) || (lepton_ID[1]==-lepton_ID[3] && abs(Mll13-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[2])) || (lepton_ID[2]==-lepton_ID[3] && abs(Mll23-91.2e3)<10e3 && (lepton_ID[0]==-lepton_ID[1]))) && (nJets_mv2c10_77==0) && (lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && (lepton_PromptLeptonVeto[3]<-0.4 && lepton_isTightLH[3]) && (abs(best_Z_other_Mll-91.2e3)>=10e3)  )"
###   MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3)"
### 
### 
### # ------------------ # 
### # --- 3L REGIONS --- # 
### # ------------------ # 
### 
### ###--- 3L preselection && exactly 1 jet ---###
### 
### Region: "three_lep_presel_1jet"
###   Type: SIGNAL
###   DataType: ASIMOV
###   Variable: "BDT3",10,-1.,1.
###   Binning: "AutoBin","TransfoD",5.,7.
###   VariableTitle: "BDT response"
###   Label: "WVZ-3l-PreSel-1Jet"
###   ShortLabel: "WVZ-3l-PreSel-1Jet"
###   Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==3) && (abs(total_charge)==1) && (lepton_PromptLeptonVeto[0]<-0.4 && lepton_isTightLH[0] && lepton_PromptLeptonVeto[1]<-0.4 && lepton_isTightLH[1] && lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3)) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3)) && (nJets_mv2c10_70==0) && (@jet_pt.size()==1)  )"
###   MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2)"

###--- 3L preselection && exactly 2 jets ---###

Region: "three_lep_presel_2jets"
  Type: SIGNAL
  DataType: ASIMOV
  Variable: "BDT4",10,-1.,1.
  Binning: "AutoBin","TransfoD",5.,7.
  VariableTitle: "BDT response"
  Label: "WVZ-3l-PreSel-2Jets"
  ShortLabel: "WVZ-3l-PreSel-2Jets"
  Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==3) && (abs(total_charge)==1) && (lepton_PromptLeptonVeto[0]<-0.4 && lepton_isTightLH[0] && lepton_PromptLeptonVeto[1]<-0.4 && lepton_isTightLH[1] && lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3)) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3)) && (nJets_mv2c10_70==0) && (@jet_pt.size()==2) && (jet_pt[0]/1000.>35.)  )"
  MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2)"

###--- 3L preselection && at least 3 jets ---###

### Region: "three_lep_presel_atLeast_3jets"
###   Type: SIGNAL
###   DataType: ASIMOV
###   Variable: "BDT5",10,-1.,1.
###   Binning: "AutoBin","TransfoD",5.,7.
###   VariableTitle: "BDT response"
###   Label: "WVZ-3l-PreSel-3Jets"
###   ShortLabel: "WVZ-3l-PreSel-3Jets"
###   Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==3) && (abs(total_charge)==1) && (lepton_PromptLeptonVeto[0]<-0.4 && lepton_isTightLH[0] && lepton_PromptLeptonVeto[1]<-0.4 && lepton_isTightLH[1] && lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3)) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3)) && (nJets_mv2c10_70==0) && (@jet_pt.size()>2)  )"
###   MCweight: "(lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2)"

# --------------- # 
# --- SAMPLES --- # 
# --------------- # 

Sample: "WVZ"
  Type: SIGNAL 
  Title: "WVZ"
  NtupleFiles: "WVZ"
  FillColor: 2
  LineColor: 1

Sample: "ZZ"
  Type: BACKGROUND
  Title: "ZZ"
  NtupleFiles: "ZZ"
  FillColor: 428
  LineColor: 1

Sample: "WZ"
  Type: BACKGROUND
  Title: "WZ"
  NtupleFiles: "WZ"
  FillColor: 46
  LineColor: 1

Sample: "lllljj"
  Type: BACKGROUND
  Title: "lllljj"
  NtupleFiles: "lllljj"
  FillColor: 4
  LineColor: 1

#--- overlapping with WVZ->3L2j, ignored by now! ---#
### Sample: "lllvjj"
###   Type: BACKGROUND
###   Title: "lllvjj"
###   NtupleFiles: "lllvjj"
###   FillColor: 11
###   LineColor: 1

Sample: "Zjets"
  Type: BACKGROUND
  Title: "Zjets"
  NtupleFiles: "Zjets"
  FillColor: 3
  LineColor: 1

Sample: "Zgamma"
  Type: BACKGROUND
  Title: "Zgamma"
  NtupleFiles: "Zgamma"
  FillColor: 876
  LineColor: 1

Sample: "ttZ"
  Type: BACKGROUND
  Title: "ttZ"
  NtupleFiles: "ttZ"
  FillColor: 8
  LineColor: 1

Sample: "tZ"
  Type: BACKGROUND
  Title: "tZ"
  NtupleFiles: "tZ"
  FillColor: 877
  LineColor: 1

Sample: "tWZ"
  Type: BACKGROUND
  Title: "tWZ"
  NtupleFiles: "tWZ"
  FillColor: 920
  LineColor: 1

Sample: "VH"
  Type: BACKGROUND
  Title: "VH"
  NtupleFiles: "VH"
  FillColor: 5
  LineColor: 1

Sample: "others"
  Type: BACKGROUND
  Title: "others"
  NtupleFiles: "others"
  FillColor: 6
  LineColor: 1

#
#Sample: "data"
#  Type: DATA
#  Title: "data"
#  NtupleFiles: "data"

# ----------------------------- # 
# --- Parameter(s) of Interest --- # 
# ----------------------------- # 

NormFactor: "mu_XS_WVZ"
  Title: "mu_XS(WVZ)"
  Nominal: 1
  Min: -10
  Max: 20
  Samples: "WVZ"

# ---- N.B. either free-floating normalisations or priors ---- #

### NormFactor: "N_WZ"
###   Title: "N(WZ)"
###   Nominal: 1
###   Min: -10
###   Max: 20
###   Samples: "WZ"
### 
### 
### NormFactor: "N_ZZ"
###   Title: "N(ZZ)"
###   Nominal: 1
###   Min: -10
###   Max: 20
###   Samples: "ZZ"


# ------------------- # 
# --- SYSTEMATICS --- # 
# ------------------- # 


# ---- N.B. either free-floating normalisations or priors ---- #
######### NORM PRIORS #########

Systematic: "WZ_XS"
  Title: "WZ_XS"
  Type: OVERALL
  OverallUp: 0.20
  OverallDown: -0.20
  Samples: "WZ" 
  Category: Theory

Systematic: "ZZ_XS"
  Title: "ZZ_XS"
  Type: OVERALL
  OverallUp: 0.20
  OverallDown: -0.20
  Samples: "ZZ" 
  Category: Theory

######### SHAPE #########

Systematic: "WZ_shape" 
  Title: "WZ shape" 
  Type: HISTO
  NtupleFileUp: "PP8_WZ"
  DropNorm: all
  Samples: "WZ"
  Regions: "three_lep_presel_1jet","three_lep_presel_2jets","three_lep_presel_atLeast_3jets"
  Category: Generators
#  Symmetrisation: TWOSIDED
  Symmetrisation: ONESIDED
  Smoothing: 40

Systematic: "ZZ_shape" 
  Title: "ZZ shape" 
  Type: HISTO
  NtupleFileUp: "PP8_ZZ"
  DropNorm: all
  Samples: "ZZ"
  Category: Generators
#  Symmetrisation: TWOSIDED
  Symmetrisation: ONESIDED
  Smoothing: 40

##### LUMINOSITY ###### 

Systematic: "ATLAS_lumi"
  Title: "ATLAS_lumi"
  Type: OVERALL
  OverallUp: 0.032
  OverallDown: -0.032
  Category: Instrumental

######### SCALE FACTORS #########

Systematic: "ATLAS_PRW_DATASF"
  Title: "ATLAS_PRW_DATASF"
  Type: HISTO
  WeightSufUp: "pileupEventWeight_UP/pileupEventWeight"
  WeightSufDown: "pileupEventWeight_DOWN/pileupEventWeight"
  Category: Instrumental

Systematic: "ATLAS_JVT"
  Title: "ATLAS_JVT"
  Type: HISTO
  WeightSufUp: "JVT_EventWeight_UP/JVT_EventWeight"
  WeightSufDown: "JVT_EventWeight_DOWN/JVT_EventWeight"
  Category: Instrumental

Systematic: "ATLAS_EL_SF_ID"
  Title: "ATLAS_EL_SF_ID"
  Type: HISTO
  WeightSufUp: "(    (  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_EL_SF_ID_UP_0*lepton_SFObjLoose_EL_SF_ID_UP_1*lepton_SFObjLoose_EL_SF_ID_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )  )      +      (  (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_EL_SF_ID_UP_0*lepton_SFObjLoose_EL_SF_ID_UP_1*lepton_SFObjLoose_EL_SF_ID_UP_2*lepton_SFObjLoose_EL_SF_ID_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )    )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_EL_SF_ID_DOWN_0*lepton_SFObjLoose_EL_SF_ID_DOWN_1*lepton_SFObjLoose_EL_SF_ID_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_EL_SF_ID_DOWN_0*lepton_SFObjLoose_EL_SF_ID_DOWN_1*lepton_SFObjLoose_EL_SF_ID_DOWN_2*lepton_SFObjLoose_EL_SF_ID_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

Systematic: "ATLAS_EL_SF_RECO"
  Title: "ATLAS_EL_SF_RECO"
  Type: HISTO
  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_EL_SF_Reco_UP_0*lepton_SFObjLoose_EL_SF_Reco_UP_1*lepton_SFObjLoose_EL_SF_Reco_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_EL_SF_Reco_UP_0*lepton_SFObjLoose_EL_SF_Reco_UP_1*lepton_SFObjLoose_EL_SF_Reco_UP_2*lepton_SFObjLoose_EL_SF_Reco_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_EL_SF_Reco_DOWN_0*lepton_SFObjLoose_EL_SF_Reco_DOWN_1*lepton_SFObjLoose_EL_SF_Reco_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_EL_SF_Reco_DOWN_0*lepton_SFObjLoose_EL_SF_Reco_DOWN_1*lepton_SFObjLoose_EL_SF_Reco_DOWN_2*lepton_SFObjLoose_EL_SF_Reco_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

### isolation SFs not yet available for PLI/PLV...
#Systematic: "ATLAS_EL_SF_ISO"
#  Title: "ATLAS_EL_SF_ISO"
#  Type: HISTO
#  WeightSufUp: "lepton_SFObjLoose_EL_SF_Isol_UP"
#  WeightSufDown: "lepton_SFObjLoose_EL_SF_Isol_DOWN"
#  Category: Instrumental

 
### no reco Eff since 2.4.22, see https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/TopPhys/xAOD/TopCorrections/Root/MuonScaleFactorCalculator.cxx#L313
#Systematic: "ATLAS_MU_SF_ID_Stat"
#  Title: "ATLAS_MU_SF_ID_Stat"
#  Type: HISTO
#  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_ID_STAT_UP_0*lepton_SFObjLoose_MU_SF_ID_STAT_UP_1*lepton_SFObjLoose_MU_SF_ID_STAT_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_ID_STAT_UP_0*lepton_SFObjLoose_MU_SF_ID_STAT_UP_1*lepton_SFObjLoose_MU_SF_ID_STAT_UP_2*lepton_SFObjLoose_MU_SF_ID_STAT_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
#  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_2*lepton_SFObjLoose_MU_SF_ID_STAT_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
#  Category: Instrumental
#
#Systematic: "ATLAS_MU_SF_ID_Syst"
#  Title: "ATLAS_MU_SF_ID_Syst"
#  Type: HISTO
#  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_ID_SYST_UP_0*lepton_SFObjLoose_MU_SF_ID_SYST_UP_1*lepton_SFObjLoose_MU_SF_ID_SYST_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_ID_SYST_UP_0*lepton_SFObjLoose_MU_SF_ID_SYST_UP_1*lepton_SFObjLoose_MU_SF_ID_SYST_UP_2*lepton_SFObjLoose_MU_SF_ID_SYST_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
#  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_2*lepton_SFObjLoose_MU_SF_ID_SYST_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
#  Category: Instrumental
 
Systematic: "ATLAS_MU_SF_RECO_Stat"
  Title: "ATLAS_MU_SF_RECO_Stat"
  Type: HISTO
  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_RECO_STAT_UP_0*lepton_SFObjLoose_MU_SF_RECO_STAT_UP_1*lepton_SFObjLoose_MU_SF_RECO_STAT_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_RECO_STAT_UP_0*lepton_SFObjLoose_MU_SF_RECO_STAT_UP_1*lepton_SFObjLoose_MU_SF_RECO_STAT_UP_2*lepton_SFObjLoose_MU_SF_RECO_STAT_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_2*lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

Systematic: "ATLAS_MU_SF_RECO_Syst"
  Title: "ATLAS_MU_SF_RECO_Syst"
  Type: HISTO
  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_RECO_SYST_UP_0*lepton_SFObjLoose_MU_SF_RECO_SYST_UP_1*lepton_SFObjLoose_MU_SF_RECO_SYST_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_RECO_SYST_UP_0*lepton_SFObjLoose_MU_SF_RECO_SYST_UP_1*lepton_SFObjLoose_MU_SF_RECO_SYST_UP_2*lepton_SFObjLoose_MU_SF_RECO_SYST_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_2*lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

Systematic: "ATLAS_MU_SF_TTVA_Stat"
  Title: "ATLAS_MU_SF_TTVA_Stat"
  Type: HISTO
  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_0*lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_1*lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_0*lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_1*lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_2*lepton_SFObjLoose_MU_SF_TTVA_STAT_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_0*lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_1*lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_2*lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

Systematic: "ATLAS_MU_SF_TTVA_Syst"
  Title: "ATLAS_MU_SF_TTVA_Syst"
  Type: HISTO
  WeightSufUp: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_0*lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_1*lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_0*lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_1*lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_2*lepton_SFObjLoose_MU_SF_TTVA_SYST_UP_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  WeightSufDown: "(  (@lepton_pt.size()==3) * ( (lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_2) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2) )      +      (@lepton_pt.size()==4) * ( (lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_0*lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_1*lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_2*lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN_3) / (lepton_SFObjLoose_0*lepton_SFObjLoose_1*lepton_SFObjLoose_2*lepton_SFObjLoose_3) )  )"
  Category: Instrumental

### isolation SFs not yet available for PLI/PLV...
#Systematic: "ATLAS_MU_SF_ISO_Stat"
#  Title: "ATLAS_MU_SF_ISO_Stat"
#  Type: HISTO
#  WeightSufUp: "lepton_SFObjLoose_MU_SF_Isol_STAT_UP"
#  WeightSufDown: "lepton_SFObjLoose_MU_SF_Isol_STAT_DOWN"
#  Category: Instrumental
#
#Systematic: "ATLAS_MU_SF_ISO_Syst"
#  Title: "ATLAS_MU_SF_ISO_Syst"
#  Type: HISTO
#  WeightSufUp: "lepton_SFObjLoose_MU_SF_Isol_SYST_UP"
#  WeightSufDown: "lepton_SFObjLoose_MU_SF_Isol_SYST_DOWN"
#  Category: Instrumental


######### THEORY #########

### tWZ ###

Systematic: "tWZ_XS"
  Title: "tWZ_XS"
  Type: OVERALL
  OverallUp: XXX_WtZ_XS_UP
  OverallDown: XXX_WtZ_XS_DO
  Samples: "tWZ"
  Category: Theory
 
### tZ ###

Systematic: "tZ_XS"
  Title: "tZ_XS"
  Type: OVERALL
  OverallUp: XXX_tZ_XS_UP
  OverallDown: XXX_tZ_XS_DO
  Samples: "tZ"
  Category: Theory

### ttZ ###

Systematic: "ttZ_XS_QCDscale"
  Title: "ttZ_XS_QCDscale"
  Type: OVERALL
  OverallUp: XXX_ttZ_XS_QCDscale_UP
  OverallDown: XXX_ttZ_XS_QCDscale_DO
  Samples: "ttZ"
  Category: Theory

Systematic: "ttZ_XS_PDFunc"
  Title: "ttZ_XS_PDFunc"
  Type: OVERALL
  OverallUp: XXX_ttZ_XS_PDFunc_UP
  OverallDown: XXX_ttZ_XS_PDFunc_DO
  Samples: "ttZ"
  Category: Theory

### Z+jets ###

Systematic: "Zjets_XS"
  Title: "Zjets_XS"
  Type: OVERALL
  OverallUp: XXX_Zjets_XS_UP
  OverallDown: XXX_Zjets_XS_DO
  Samples: "Zjets"
  Category: Theory

### VH ###

Systematic: "VH_XS"
  Title: "VH_XS"
  Type: OVERALL
#  OverallUp: XXX_VH_XS_UP
#  OverallDown: XXX_VH_XS_DO
  OverallUp: 0.30 
  OverallDown: -0.30 
  Samples: "VH"
  Category: Theory

