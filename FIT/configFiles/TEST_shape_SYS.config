# WVZ fit
# Andrea Sciandra 
# andrea.sciandra@cern.ch

#--------------- # 
#---  JOB    --- # 
#--------------- # 

Job: "understand_shape_SYS_parabolic_linear"
#Job: "understand_shape_SYS_10"
#Job: "understand_shape_SYS_80"
  CmeLabel: "13 TeV"
  POI: "mu_XS_WVZ"
  ReadFrom: NTUP
  NtuplePaths: "/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/4L_and_3L_six_BDTs_output/"
  LumiLabel: "80.0 fb^{-1}"
  Lumi: 79888.3
#  LumiLabel: "10.0 fb^{-1}"
#  Lumi: 10000.0
  MCweight: "(mcWeightOrg*xsec*(1./tot_weight)*pileupEventWeight*JVT_EventWeight)"
  PlotOptions: "YIELDS","NORMSIG"
  NtupleName: "nominal"
  DebugLevel: 9
  BlindingThreshold: 0.1
  SystControlPlots: TRUE
  SystErrorBars: TRUE
  SystPruningShape: 0.005
  SystPruningNorm: 0.005
  SystLarge: 1
  CorrelationThreshold: 0.05
  RankingMaxNP: 30
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE
  MCstatThreshold: 0.90
  ImageFormat: "png"
  Selection: "1"
  ReplacementFile: my_Common_XS_unc.txt

# --------------- # 
# ---  FIT    --- # 
# --------------- # 

Fit: "fit"
  UseMinos: "all"
#  UseMinos: "mu_XS_WVZ"
  FitType: SPLUSB
  FitRegion: CRSR
  POIAsimov: 1
  FitBlind: TRUE
  doLHscan: "mu_XS_WVZ"

# --------------- # 
# ---  LIMIT  --- # 
# --------------- # 

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  POIAsimov: 1 

# --------------- # 
# --- REGIONS --- # 
# --------------- # 

###--- 3L preselection && exactly 1 jet ---###

Region: "three_lep_presel_1jet"
  Type: SIGNAL
  DataType: ASIMOV
  Variable: "BDT3",10,-1.,1.
  Binning: "AutoBin","TransfoD",5.,7.
  VariableTitle: "BDT response"
  Label: "WVZ-3l-PreSel-1Jet"
  ShortLabel: "WVZ-3l-PreSel-1Jet"
  Selection: "(  (passEventCleaning && passTrigger && isTrigMatched) && (@lepton_pt.size()==3) && (abs(total_charge)==1) && (lepton_PromptLeptonVeto[0]<-0.4 && lepton_isTightLH[0] && lepton_PromptLeptonVeto[1]<-0.4 && lepton_isTightLH[1] && lepton_PromptLeptonVeto[2]<-0.4 && lepton_isTightLH[2]) && ((lepton_ID[0]!=-lepton_ID[1] || Mll01>12e3) && (lepton_ID[0]!=-lepton_ID[2] || Mll02>12e3) && (lepton_ID[1]!=-lepton_ID[2] || Mll12>12e3)) && ((lepton_ID[0]==-lepton_ID[1] && abs(Mll01-91.2e3)<10e3) || (lepton_ID[0]==-lepton_ID[2] && abs(Mll02-91.2e3)<10e3) || (lepton_ID[1]==-lepton_ID[2] && abs(Mll12-91.2e3)<10e3)) && (nJets_mv2c10_70==0) && (@jet_pt.size()==1)  )"

# --------------- # 
# --- SAMPLES --- # 
# --------------- # 

Sample: "WVZ"
  Type: SIGNAL 
  Title: "WVZ"
  NtupleFiles: "WVZ"
  FillColor: 2
  LineColor: 1

Sample: "WZ"
  Type: BACKGROUND
  Title: "WZ"
  NtupleFiles: "WZ"
  FillColor: 46
  LineColor: 1

# ----------------------------- # 
# --- Parameter of Interest --- # 
# ----------------------------- # 

NormFactor: "mu_XS_WVZ"
  Title: "mu_XS(WVZ)"
  Nominal: 1
  Min: -10
  Max: 20
  Samples: "WVZ"

### NormFactor: "N_WZ"
###   Title: "N(WZ)"
###   Nominal: 1
###   Min: -10
###   Max: 20
###   Samples: "WZ"

# ------------------- # 
# --- SYSTEMATICS --- # 
# ------------------- # 

######### SHAPE #########

Systematic: "WZ_shape" 
  Title: "WZ shape" 
  Type: HISTO
  NtupleFileUp: "PP8_WZ"
  DropNorm: all
  KeepNormForSamples: "WZ"
  Samples: "WZ"
  Category: Generators
#  Symmetrisation: TWOSIDED
  Symmetrisation: ONESIDED
  Smoothing: 40

#---- linear variation ----# 

Systematic: "VV_shape1"
  Title: "Diboson linear"
  Type: HISTO
  WeightSufUp: "(1.+0.30*BDT3/1)*((1.+0.30*BDT3/1)>0.)+1.e-06*((1.+0.30*BDT3/1)<=0.)"
  WeightSufDown: "(1.-0.30*BDT3/1)*((1.-0.30*BDT3/1)>0.)+1.e-06*((1.-0.30*BDT3/1)<=0.)"
  DropNorm: all
  Samples: "WZ"
  Category: ShapeTheory
  Smoothing: 40

#---- parabolic variation ----#

Systematic: "VV_shape2"
  Title: "Diboson parabolic"
  Type: HISTO
  WeightSufUp: "(1.-0.30+2*0.30*(BDT3/1)*(BDT3/1))*((1.-0.30+2*0.30*(BDT3/1)*(BDT3/1))>0.)+1.e-06*((1.-0.30+2*0.30*(BDT3/1)*(BDT3/1))<=0.)"
  WeightSufDown: "(1.+0.30-2*0.30*(BDT3/1)*(BDT3/1))*((1.+0.30-2*0.30*(BDT3/1)*(BDT3/1))>0.)+1.e-06*((1.+0.30-2*0.30*(BDT3/1)*(BDT3/1))<=0.)"
  DropNorm: all
  Samples: "WZ"
  Category: ShapeTheory
  Smoothing: 40

