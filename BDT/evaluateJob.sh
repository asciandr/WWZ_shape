#!/bin/bash

pwd
workdir=`pwd`
mydir=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/skimmed_v7/data/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/training_v7/MC16d/
input_folder=/eos/atlas/user/a/asciandr/WWZ/training_v7/MC16a/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/skimmed_v7/MC16a/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/skimmed_v7/MC16d/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/wFlatBranches_full_syst/
#input_folder=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/Skim/sys_test5/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/hadded_wFlatBranches_skimmed_v6/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/hadded_TightLHSF_SYST_MC16_wFlatBranches_skimmed_v5.0/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/hadded_TightLHSF_SYST_MC16_wFlatBranches_skimmed_v3.1/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/data_wFlatBranches_skimmed_v3.0/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v3.1/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.1/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/wFlatBranches_skimmed_v2.0/
#input_folder=/eos/atlas/user/a/asciandr/WWZ/MC16a_3TightLep_atLeast1Z_wFlatBranches_v2.0/
echo XSAMPTXT
cp $mydir/*.C $mydir/*.py $mydir/*.h $mydir/*.sh .
cp -r $mydir/WVZ/ .
ls -haltr 
. setup.sh
ls -haltr $input_folder/XFILE
ls -1 $input_folder/XFILE > XSAMPTXT
python run_application.py XSAMPTXT WVZ/app_selection WVZ/weight.txt WVZ/app_input_xml WVZ/app_outFolder
echo 'done.'
