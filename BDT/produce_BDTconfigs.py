#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

configFile 	= ""
basefile	= ""

try:
    configFile 	= str(sys.argv[1])
    basefile    = str(sys.argv[2])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python produce_BDTconfigs.py parameters.txt WWZ/input.varlist"
    sys.exit(1)
    pass

if not os.path.exists("./"+configFile): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+basefile): 
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass

#Retrieve input values
pars=[]
steps=[]
mins=[]
maxs=[]

print "Opening .txt file '"+configFile+"'"
txt_file  = open(configFile,"r")
for line in txt_file:
    if(line.startswith("#")): 
        continue
    else:
        indexA=line.find('\t')
        #print line[0:indexA]
        pars.append(line[0:indexA])
        indexB=line.find(';')
        line2=line[indexB:len(line)]
        indexC=line2.find('\t')
        #print line2[1:indexC]
        steps.append(line2[1:indexC])
        line3=line2[indexC:len(line2)]
        indexD=line3.find(';') 
        line4=line3[indexD:len(line3)]
        indexE=line4.find('\t')
        #print line4[1:indexE]
        mins.append(line4[1:indexE])
        line5=line4[indexE:len(line4)]
        indexF=line5.find(';')
        #print line5[indexF+1:line5.find('   ')]
        maxs.append(line5[indexF+1:line5.find('   ')])
        #print "next parameter"
        
print ""
print "List of parameters, steps, mins and maxs:"
print ""
print pars
print steps
print mins
print maxs
print ""

#Save all combinations btw mins and maxs
allntrees=[]
allMNS=[]
allshrinkage=[]
allncuts=[]
allMD=[]

for i,x in enumerate(pars):
    STEP=int(steps[i])
    MIN=int(mins[i])
    MAX=int(maxs[i])
    if x.find("NTrees") != -1: 
        while (MIN<=MAX):
            allntrees.append(MIN)
            MIN=MIN+STEP
#        print x
    elif x.find("MinNodeSize") != -1:
        while (MIN<=MAX):
            allMNS.append(MIN)
            MIN=MIN+STEP
#        print x
    elif x.find("Shrinkage") != -1:
        while (MIN<=MAX):
            allshrinkage.append(MIN)
            MIN=MIN+STEP
#        print x
    elif x.find("NCuts") != -1:
        while (MIN<=MAX):
            allncuts.append(MIN)
            MIN=MIN+STEP
#        print x
    elif x.find("MaxDepth") != -1:
        while (MIN<=MAX):
            allMD.append(MIN)
            MIN=MIN+STEP
#        print x
        
print allntrees
print allMNS 
print allshrinkage
print allncuts
print allMD

#Write all needed varlist files
#outFolder="manyVarlists/"
outFolder=""
varlistname=""
for a in allntrees:
    for b in allMNS:
        for c in allshrinkage:
            for d in allncuts:
                for e in allMD:
		    #Shrinkage is peculiar...
		    c_str=""
                    shrink_length=len(str(c))
                    if shrink_length==1:
                        c_str="00"+str(c)
                    elif shrink_length==2:
                        c_str="0"+str(c)
                    else:
                        print "#WARNING# CHECK ME CAREFULLY!!!"
                    varlistname=""
                    varlistname+="myvarlist_NTrees"+str(a)+"_MinNodeSize"+str(b)+"_Shrinkage"+c_str+"_NCuts"+str(d)+"_MaxDepth"+str(e)+"_BDT.varlist"
		    command="cp "+basefile+" "+outFolder+varlistname
                    os.system(command) 
#                    print varlistname
#                    print command
		    
     
print ""
print "********************		ALL VARLIST ARE READY TO BE SENT ON BATCH		********************"
print ""
