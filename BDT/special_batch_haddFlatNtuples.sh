#!/bin/bash


#workdir=`pwd`

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

setupATLAS --quiet

#INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16a/
INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16d/

#HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16a/
HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/

echo "Going to HADD flat ntuples in:	"${HADD_FOLDER}

# ls -haltr

#listtxt=$*
#COUNTER=0
#while read sample
#do
#    #substring=$(echo ${sample} | cut -d '_' -f 2 | cut -d '.' -f 2)
sample=364250.root

#/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/complete_364250.root
#/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/nominal_364250.root

xrdcp -f root://eosatlas.cern.ch/${HADD_FOLDER}/complete_364250.root .
xrdcp -f root://eosatlas.cern.ch/${HADD_FOLDER}/nominal_364250.root .

cp /afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/startup.C .

root.exe -b -l -q startup.C+
LD_PRELOAD=${workdir}/startup_C.so hadd -k ${sample} complete_364250.root nominal_364250.root

xrdcp -f ${sample} root://eosatlas.cern.ch/${HADD_FOLDER}/really_complete_${sample}

#echo ${sample}    
#echo ${INPUT_FOLDER}/*_${sample}
#echo ${HADD_FOLDER}/${sample}
#xrdcp root://eosatlas.cern.ch/${HADD_FOLDER}/${sample} .
#mv ${sample} old_${sample}
#hadd -k -n 5 ${sample} old_${sample} ${INPUT_FOLDER}/*_${sample}
#ls -haltr ${sample}
#echo "Copying to eos:"
##xrdcp -f -d 3 ${sample} ${HADD_FOLDER}/${sample}
#xrdcp -f ${sample} root://eosatlas.cern.ch/${HADD_FOLDER}/complete_${sample}
#xrdcp -f ${sample} root://eosatlas.cern.ch//eos/atlas/user/a/asciandr/WWZ/${sample}
#COUNTER=$[${COUNTER}+1]
#done < $*
