#!/bin/bash

MIN_VARS=6
MAX_VARS=11
listtxt=$*
COUNTER=0
while read config
do
    echo $config
    OCC=$(echo "${config}" | awk -F1 '{print NF-1}')
    echo 'Number of 1 occurences:'
    echo $OCC
    if [[ "$OCC" -ge "$MIN_VARS" && "$OCC" -le "$MAX_VARS" ]]
    then
        echo 'Good permutation, I ll launch the batch job'
        echo $OCC
        cat trainJob.sh | sed "s/CONFIG/$config/g" > trainJob_$COUNTER.sh 
        cat run_on_batch.py | sed "s/MINIMUM_VARIABLES/$OCC/g" | sed "s/MAXIMUM_VARIABLES/$OCC/g" > run_on_batch_$config.py
        cat WVZ/input.varlist | grep -v '#' > input_woHash.varlist
        SECONDCOUNTER=0
        while read line
        do
#            echo $line
            CONFIGCHAR=${config:${SECONDCOUNTER}:1}
            if [[ $CONFIGCHAR == '1' ]]
            then
                echo $line >> input_$config.varlist
            fi
            SECONDCOUNTER=$[$SECONDCOUNTER+1]
        done < input_woHash.varlist
        chmod 755 trainJob_$COUNTER.sh 
        queue="8nh"
        bsub -R "rusage[mem=4096]" -q $queue trainJob_$COUNTER.sh 
        echo $queue
        COUNTER=$[$COUNTER+1]
    fi
done < $*
