#!/bin/bash

listtxt=$*
COUNTER=0
while read config
do
    echo $config
    cat job.sh | sed "s/CONFIG/$config/g" > job_$COUNTER.sh 
    chmod 755 job_$COUNTER.sh 
    queue="8nh"
    bsub -q $queue job_$COUNTER.sh 
    echo $queue
    COUNTER=$[$COUNTER+1]
done < $*
