#!/bin/bash

listtxt=$*
while read samp
do
    echo $samp
    sampname=`echo $samp| sed "s/.root//g" `
    echo $sampname
    cat batch_haddFlatNtuples.sh | sed "s/XSAMPNAME/$sampname/g" > batch_haddFlatNtuples_$sampname.sh
    chmod 755 batch_haddFlatNtuples_$sampname.sh
    echo batch_haddFlatNtuples_$sampname.sh
#    queue="8nh"
#    queue="1nd"
    queue="2nd"
    bsub -R "pool>400000" -q $queue batch_haddFlatNtuples_$sampname.sh
    echo $queue
done < $*
