#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

configFile 	= ""
variables 	= ""
selection 	= ""
weight 		= ""
myConfiguration	= ""

try:
    configFile 	= str(sys.argv[1])
    variables 	= str(sys.argv[2])
    selection 	= str(sys.argv[3])
    weight 	= str(sys.argv[4])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py configFile variables selection weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+configFile): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+variables):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[4])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L auto_OptimisationTMVAClassification.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

#--- Extract BDT settings from filename ---#
if variables.find("NTrees") != -1:
    print ""
    print "#WARNING# Your input files name contains input settings for the BDT -> I'll catch them"
    print ""
    myConfiguration+="!H:!V:" 
    #string stuff... 
    indexA=variables.find('NTrees')+6
    indexB=(variables[indexA:indexA+5]).find('_')
    ntrees=variables[indexA:indexA+indexB]
    indexA=variables.find('NCuts')+5
    indexB=(variables[indexA:indexA+5]).find('_')
    ncuts=variables[indexA:indexA+indexB]
    indexA=variables.find('MaxDepth')+8
    indexB=(variables[indexA:indexA+5]).find('_')
    maxdepth=variables[indexA:indexA+indexB]
    indexA=variables.find('MinNodeSize')+11
    indexB=(variables[indexA:indexA+5]).find('_')
    minnodesize=variables[indexA:indexA+indexB]
    indexA=variables.find('Shrinkage')+9
    indexB=(variables[indexA:indexA+5]).find('_')
    shrinkage=variables[indexA]+"."+variables[indexA+1:indexA+indexB]
    myConfiguration+="NTrees="+ntrees+":MinNodeSize="+minnodesize+"%:BoostType=Grad:Shrinkage="+shrinkage+":UseBaggedBoost:BaggedSampleFraction=0.5:nCuts="+ncuts+":MaxDepth="+maxdepth+":IgnoreNegWeightsInTraining"
else:
#   DF baseline configuration
#    myConfiguration="!H:!V:NTrees=500:MinNodeSize=15%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=10:MaxDepth=2:IgnoreNegWeightsInTraining"
#   SF baseline configuration
#    myConfiguration="!H:!V:NTrees=1000:MinNodeSize=5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=30:MaxDepth=5:IgnoreNegWeightsInTraining"
#   ON-shell SF baseline configuration
    myConfiguration="!H:!V:NTrees=2600:MinNodeSize=3%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=140:MaxDepth=2:IgnoreNegWeightsInTraining"

myMethodList=""
min_N=15 
max_N=15 
#--- Set integrated luminosity ---#
Lum=1.
#Lum=79888.3
analysis="4lep"
debug=true
onlyttZ=false

print ""
print"**********	OPTIONS SELECTED FOR THIS TRAINING	**********"
print "configFile="+configFile+"	myMethodList="+myMethodList+"		min_N="+str(min_N)+"		max_N="+str(max_N)+"		Lum="+str(Lum)+"	analysis="+analysis+"		selection="+selection+"		DEBUG mode="+str(debug)+"	onlyttZ="+str(onlyttZ)+"		weight="+weight
print ""
print "BDT configuration:	"+myConfiguration
print"**********	OPTIONS SELECTED FOR THIS TRAINING	**********"
print ""

auto_OptimisationTMVAClassification( configFile, myConfiguration, variables, selection, weight, myMethodList, min_N, max_N, Lum, analysis, debug, onlyttZ )

print ""
print "********************		DONE		********************"
print ""
