#!/bin/bash

pwd
workdir=`pwd`
echo 'This is the configuration:'
echo CONFIG
mydir=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/
cp $mydir/*.C $mydir/*.py $mydir/*.h $mydir/*.sh .
cp $mydir/CONFIG .
cp -r $mydir/WVZ/ .
ls -haltr
. setup.sh
# eos not working...
#python run.py WVZ/config_DF_WWZ4Ltraining_skimmed WVZ/SF_NTrees1000_MinNodeSize5_Shrinkage010_NCuts30_MaxDepth5_BDT.varlist WVZ/selection.txt WVZ/weight.txt
python run.py WVZ/config_SF CONFIG WVZ/selection.txt WVZ/weight.txt
echo 'done.'
