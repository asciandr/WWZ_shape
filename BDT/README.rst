Setup environment     
---------
I.e. ::

  source setup.sh

Run BDT training
---------
...with settings defined in the python script, e.g.:: 

  nohup python run.py WVZ/config_SF WVZ/input.varlist WVZ/selection.txt WVZ/weight.txt >& log &
  tail -f log

...with settings defined through the varlist file name, e.g.::

  nohup python run.py WVZ/config_SF WVZ/SF_NTrees1000_MinNodeSize5_Shrinkage010_NCuts30_MaxDepth5_BDT.varlist WVZ/selection.txt WVZ/weight.txt >& log &
  tail -f log

Run BDT evaluation
---------
Make sure that your xml inputs are mapped to MakeClass methods in ``app_header.h``. Then test locally, e.g.::

  nohup python run_application.py WVZ/app_config WVZ/app_selection WVZ/weight.txt WVZ/app_input_xml WVZ/app_outFolder >& log &
  tail -f log

and (you better) submit batch jobs (1 job per inpur root file), e.g. ::

  ./evaluation_on_batch.sh app_3L_skimmed_config_batch

Merge huge full-systematic fit inputs
---------
Merge one-by-one full-systematic ntuple TTrees to be fed as inputs to the fit, e.g. ::

  ./merge_fit_inputs.sh to_be_merged

Run optimisation of BDT input variables in batch
---------
Clean everything (make sure old ``LSFJOB_*`` folders are not there)::

  . clean.sh

Produce all permutations of 0 and 1 for a string of length equal to the number of variables defined in ``WVZ/input.varlist``::

  root -l produce_permutations.C\(\"WVZ/input.varlist\"\)
  .q

Choose in ``train_on_batch.sh`` the minimum (``MIN_VARS``) and maximum (``MAX_VARS``) number of inputs you want for the BDT training and launch one batch job per each training::

  ./train_on_batch.sh permutations.txt

Once all bjobs are done find the best configuration::

  nohup python find_bestroc.py >& log &
  tail -f log

and plot the ROC integral for all different sets of input variables thanks to ``stupidPlot.C``::

  root -l stupidPlot.C

Run optimisation of BDT parameters in batch
---------
Clean everything (make sure old ``LSFJOB_*`` folders are not there)::

  . clean.sh

Choose in ``parameters.txt`` ranges and steps for each BDT parameter and produce copies of a varlist with the defined combination of settings::

  nohup python produce_BDTconfigs.py parameters.txt WVZ/input.varlist >& log &
  tail -f log

List them in a txt file:: 

  ls -1 myvarlist_NTrees* >& test.txt

and submit all jobs to the batch::	

  ./submit_batch.sh test.txt

Once all bjobs are done find the best configuration::

  nohup python find_bestroc.py >& log &
  tail -f log
