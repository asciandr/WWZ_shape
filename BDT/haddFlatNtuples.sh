#!/bin/bash


#workdir=`pwd`

setupATLAS
lsetup root 

INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16a/
#INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16d/

HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16a/
#HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/

echo "Going to HADD flat ntuples in:	"${HADD_FOLDER}

# ls -haltr

listtxt=$*
COUNTER=0
while read sample
do
    #substring=$(echo ${sample} | cut -d '_' -f 2 | cut -d '.' -f 2)
    echo "Hadding:"
    echo ${sample}    
    echo "Inputs:"
    echo ${INPUT_FOLDER}/*_${sample}
    echo "Output:"
    echo ${HADD_FOLDER}/${sample}
    hadd -f2 ${HADD_FOLDER}/${sample} ${INPUT_FOLDER}/*_${sample}
    COUNTER=$[${COUNTER}+1]
done < $*
