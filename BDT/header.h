///////////////////////// -*- C++ -*- /////////////////////////////
// Header file
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>

using namespace std;

//Inv Mass computation
float invMass(TLorentzVector l1, TLorentzVector l2) {
  float mass(-99.);
  TLorentzVector l3 = l1+l2; 
   
  return l3.M();
}

//deltR computation
float deltaR(float eta1, float eta2, float phi1, float phi2) {
  float DEta = fabs(eta1 - eta2);
  float DPhi = acos(cos(fabs(phi1 - phi2)));
  return sqrt(pow(DEta, 2) + pow(DPhi, 2));
}

/* Fill Histos */
static THStack *fillHistos( nominal *nTree, double weight, TH1F* hm3l, TH1F* h_etmiss, TH1F* h_jets, TH1F* h_jet_pt, TH1F* h_l1_pt, TH1F* h_l2_pt, TH1F* h_l3_pt, TH1F* h_m2l_Z, TH1F *h_special_m2l ) {
  //3l invariant mass
  TLorentzVector l1;
  TLorentzVector l2;
  TLorentzVector l3;
  l1.SetPtEtaPhiE(nTree->lepton_pt->at(0),nTree->lepton_eta->at(0),nTree->lepton_phi->at(0),nTree->lepton_E->at(0));
  l2.SetPtEtaPhiE(nTree->lepton_pt->at(1),nTree->lepton_eta->at(1),nTree->lepton_phi->at(1),nTree->lepton_E->at(1));
  l3.SetPtEtaPhiE(nTree->lepton_pt->at(2),nTree->lepton_eta->at(2),nTree->lepton_phi->at(2),nTree->lepton_E->at(2));
  
  hm3l->Fill( (l1+l2+l3).M()/1000.,weight );
  h_etmiss->Fill( nTree->MET_RefFinal_et/1000.,weight );
  h_jets->Fill( nTree->m_jet_pt->size(),weight );
  for(int a=0; a<nTree->m_jet_pt->size(); a++) h_jet_pt->Fill( nTree->m_jet_pt->at(a)/1000.,weight );
  
  // Leptons' pT in pT order
  float pT[3] ;
  int   ind[3];
  for ( int i=0; i<3; i++ ) {
    pT[i] = -99.;
    ind[i]= -99.; 
  }
  //for ( int i=0; i<nTree->lepton_pt->size(); i++) cout<<"Initial leps : "<< nTree->lepton_pt->at(i) <<endl;
  //cout<<endl;
  for ( int i=1; i<nTree->lepton_pt->size(); i++) {
    if( i==1 ) {
      pT[0]  =  nTree->lepton_pt->at(0)/1000.;
      ind[0] = 0;
    }
    if (nTree->lepton_pt->at(i)/1000.>pT[0]) {
      pT[0]  = nTree->lepton_pt->at(i)/1000.;
      ind[0] = i;
    }
  }
  bool first(true);
  for ( int i=0; i<nTree->lepton_pt->size(); i++) {
    if( i!=ind[0]&&first ) {
      pT[1]  = nTree->lepton_pt->at(i)/1000.;
      ind[1] = i;
    }
    if( i!=ind[0]&&i!=ind[1]&&nTree->lepton_pt->at(i)/1000.>pT[1]&&!first ) {
      pT[1]  = nTree->lepton_pt->at(i)/1000.;
      ind[1] = i;
    }
    first = false;
  }
  for ( int i=0; i<nTree->lepton_pt->size(); i++) {
    if( i!=ind[0]&&i!=ind[1] ) {
      pT[2]  = nTree->lepton_pt->at(i)/1000.;
      ind[2] = i;
    }
  }
  //Don't want to order them in pT anymore...
  h_l1_pt->Fill( nTree->lepton_pt->at(0)/1000.,weight );
  h_l2_pt->Fill( nTree->lepton_pt->at(1)/1000.,weight );
  h_l3_pt->Fill( nTree->lepton_pt->at(2)/1000.,weight );
  //for(int i=0; i<3; i++) cout<<"Ordered leps: ind "<< ind[i] <<" and pT = "<< pT[i]<<endl;
  
  // Invariant mass of the 2l closer to Z-peak //
  // ! the first lepton is OS ! //
  int OSSF(0.);
  double M_Z(-999.);
  double diff1(-999.), diff2(-999.);
  for(int a=1; a<nTree->lepton_pt->size(); a++) {
    if((nTree->lepton_ID->at(0)+nTree->lepton_ID->at(a)==0)){
      OSSF ++;
      TLorentzVector l1, l2;
      double Mass(-99);
      l1.SetPtEtaPhiE(nTree->lepton_pt->at(0),nTree->lepton_eta->at(0),nTree->lepton_phi->at(0),nTree->lepton_E->at(0));
      l2.SetPtEtaPhiE(nTree->lepton_pt->at(a),nTree->lepton_eta->at(a),nTree->lepton_phi->at(a),nTree->lepton_E->at(a));
      Mass = (l1+l2).M();
      //cout<< nTree->lepton_ID->at(0) <<"  "<< nTree->lepton_ID->at(a) <<endl;
      //cout<< Mass <<endl;
      if(OSSF==1) {
	diff1 = abs(Mass/1000.-91.);
       	M_Z = Mass;
      }
      if(OSSF==2) diff2 = abs(Mass/1000.-91.);
      if((OSSF==2)&&(diff2<diff1)) M_Z = Mass;
    }
  }
  if(M_Z!=-999.) h_m2l_Z->Fill(M_Z/1000.,weight);
  
  //Special plot - OSSF l pair invariant mass for events with M_3l < 91 GeV
  if((l1+l2+l3).M()/1000.<91.) h_special_m2l->Fill( M_Z/1000.,weight );
    
  return 0;
}

