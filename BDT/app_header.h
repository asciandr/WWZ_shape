#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <cstring>

#include "nominal.C"

#ifndef __CINT__
// FLOAT
typedef float (nominal::*nominal_method_F);
typedef map<string, nominal_method_F> nominal_func_map_F;
// INT
typedef int (nominal::*nominal_method_I);
typedef map<string, nominal_method_I> nominal_func_map_I;
#endif

// Map nominal methods to names
// FLOAT
static nominal_func_map_F map_functions_F () {
  nominal_func_map_F m_F;
  m_F["best_Z_Mll"]               	= &nominal::best_Z_Mll;
  m_F["best_Z_other_Mll"]               = &nominal::best_Z_other_Mll;
  m_F["Mllll0123"]                      = &nominal::Mllll0123;
  m_F["MET_RefFinal_et"]		= &nominal::MET_RefFinal_et;
  m_F["MET"]                		= &nominal::MET;
  m_F["HT_lep"]                         = &nominal::HT_lep;
  m_F["HT_had"]                         = &nominal::HT_had;
  m_F["HT"]                  		= &nominal::HT;
  m_F["Mlll012"]			= &nominal::Mlll012;        
  m_F["Mll01"]				= &nominal::Mll01;          
  m_F["Mll02"]				= &nominal::Mll02;          
  m_F["Mll12"]				= &nominal::Mll12;          
  m_F["leadJet_pt"]			= &nominal::leadJet_pt;     
  m_F["subleadJet_pt"]			= &nominal::subleadJet_pt;  
  m_F["leadLep_pt"]			= &nominal::leadLep_pt;     
  m_F["subleadLep_pt"]			= &nominal::subleadLep_pt;  
  m_F["thirdleadLep_pt"]		= &nominal::thirdleadLep_pt;
  m_F["best_W_Mjj"]			= &nominal::best_W_Mjj;     
  m_F["dPhi_Wjj_Zll"]			= &nominal::dPhi_Wjj_Zll;   
  m_F["dR_Wjj_Zll"]			= &nominal::dR_Wjj_Zll;     
  m_F["dPhi_Wlv_Zll"]			= &nominal::dPhi_Wlv_Zll;   
  m_F["M_T_Wlv"]			= &nominal::M_T_Wlv;        
  m_F["dPhi_Wjj_Wlv"]			= &nominal::dPhi_Wjj_Wlv;   
  m_F["Mjj01"]				= &nominal::Mjj01;
  m_F["Mjj02"]				= &nominal::Mjj02;
  m_F["Mjj12"]				= &nominal::Mjj12;
  m_F["smallest_Mjj"]			= &nominal::smallest_Mjj;
  m_F["jet_tot_invMass"]		= &nominal::jet_tot_invMass;
  m_F["leptons_jets_MET_tot_invMass"]	= &nominal::leptons_jets_MET_tot_invMass;
  m_F["Zll_leadJet_invMass"]		= &nominal::Zll_leadJet_invMass;
 
  return m_F;
}
// INT
static nominal_func_map_I map_functions_I () {
  nominal_func_map_I m_I;
  m_I["nJets_OR"]               	= &nominal::nJets_OR;
  m_I["nJets_mv2c10_85"]            	= &nominal::nJets_mv2c10_85;
  m_I["total_charge"]              	= &nominal::total_charge;

  return m_I;
}
