#!/bin/bash

pwd
workdir=`pwd`
echo 'This is the configuration:'
echo CONFIG
mydir=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/BDT/
cp $mydir/*.C $mydir/*.py $mydir/*.h $mydir/*.sh .
cp $mydir/input_CONFIG.varlist .
cp -r $mydir/WVZ/ .
ls -haltr
. setup.sh
python run_on_batch_CONFIG.py WVZ/config_SF input_CONFIG.varlist WVZ/selection.txt WVZ/weight.txt
ls -haltr
echo 'done.'
