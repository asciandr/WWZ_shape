#!/bin/bash


#workdir=`pwd`

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

setupATLAS --quiet

#INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/sys_MC16a/
#INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16a/
#INPUT_FOLDER=/eos/atlas/user/a/asciandr/WWZ/bdt_v7/MC16d/
#INPUT_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/
INPUT_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/VH_MC16a/
#INPUT_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/VH_MC16d/

#HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16a/
#HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/MC16d/
HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/VH_MC16a/
#HADD_FOLDER=/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_fit_inputs/VH_MC16d/

echo "Going to HADD flat ntuples in:	"${HADD_FOLDER}

dsid=XSAMPNAME
sample=${dsid}.root
echo "Hadding:"
echo ${sample}    
echo "Inputs:"
echo ${INPUT_FOLDER}/*_${sample}
echo "Output:"
echo ${HADD_FOLDER}/${sample}
for f in ${INPUT_FOLDER}/*
do
    if [[ $f = *${dsid}* ]];
    then
        xrdcp -f root://eosatlas.cern.ch/$f .
    else
        continue
    fi
done
hadd -k -n 5 ${sample} *_${sample}
ls -haltr ${sample}
echo "Copying to eos:"
xrdcp -f ${sample} root://eosatlas.cern.ch/${HADD_FOLDER}/${sample}
