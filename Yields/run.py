#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
selection = ""
weight = ""

try:
    listRootFiles = str(sys.argv[1])
    selection = str(sys.argv[2])
    weight = str(sys.argv[3])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles selection weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L giveMeYields.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

# Set integrated luminosity
# int lumi in weight for full dataset
Lum=1.
# 2015
##Lum=3219.56
# 2016 13 TeV reprocessing with R21
##Lum=32965.3
# 2017
##Lum=43805.9
# 2015 + 2016 13 TeV reprocessing with R21
##Lum=36184.86
# 2016 + 2017 13 TeV R21
##Lum=76771.2
# 2015 + 2016 + 2017 13 TeV R21
##Lum=79990.76


giveMeYields( listRootFiles, selection, weight, Lum )

