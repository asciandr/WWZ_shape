Setup environment
------
Setup ATLAS and root::

  . setup.sh

Run tool computing yields in a given region, e.g.::

  nohup python run.py file_lists/WWZ_flatntuples.txt selection.txt weight.txt >& log &
  tail -f log
