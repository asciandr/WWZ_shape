v5.0 364250				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_wFlatBranches_skimmed_v5.0/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v5.0/364250.root
v3.1 364250				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v3.1/364250.root
v5.0 364253				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_wFlatBranches_skimmed_v5.0/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v5.0/364253.root
v3.1 364253				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v3.1/364253.root
v5.0 363507				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_wFlatBranches_skimmed_v5.0/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v5.0/363507.root
v3.1 363507				;/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/TightLHSF_SYST_MC16a_wFlatBranches_skimmed_v3.1/363507.root
##################################
### $WVZ$                           	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/WVZ.root
### $\ell\ell\ell\ell$              	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/ZZ.root
### $\ell\ell\ell\nu$               	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/WZ.root
### \ell\ell\ell\ell jj         		;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/lllljj.root
### \ell\ell\ell\nu jj          		;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/lllvjj.root
### $Z+j$                           	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/Zjets.root
### $Z+\gamma$                      	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/Zgamma.root
### $t\bar{t}Z$                     	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/ttZ.root
### $tZ$                            	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/tZ.root
### $tWZ$                           	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/tWZ.root
### $VH$                            	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/VH.root
### others                          	;/eos/atlas/user/a/asciandr/WWZ/MC16a_wFlatBranches_skimmed_v3.0/others.root
### ### data 2015+2016				;/eos/atlas/user/a/asciandr/WWZ/data_wFlatBranches_skimmed_v3.0/data_2015_2016.root
