#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
variables = ""
selection = ""
weight = ""

try:
    listRootFiles = str(sys.argv[1])
    variables = str(sys.argv[2])
    selection = str(sys.argv[3])
    weight = str(sys.argv[4])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles variables region(s) weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+variables):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[4])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L giveMePlots.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

#Set blinding threshold: blind bins where the S/B is above this % in MC 
BlindThreshold=1.
#BlindThreshold=0.
#BlindThreshold=0.15
#BlindThreshold=0.10

# Set integrated luminosity
# int lumi in weight for full dataset
Lum=1.
# 2015
##Lum=3219.56
# 2016 13 TeV reprocessing with R21
##Lum=32965.3
# 2017
##Lum=43805.9
# 2015 + 2016 13 TeV reprocessing with R21
##Lum=36184.86
# 2016 + 2017 13 TeV R21
##Lum=76771.2
# 2015 + 2016 + 2017 13 TeV R21
##Lum=79990.76

# Set log scale on the y axis?
LogY=False
#LogY=True

giveMePlots( listRootFiles, variables, selection, weight, Lum, LogY, BlindThreshold )

