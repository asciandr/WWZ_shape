Setup environment
------
Setup ATLAS and root::

  . setup.sh

Run plotting tool, e.g.::

  nohup python run.py file_list/test.txt variables.txt regions.txt weight.txt >& log &
  tail -f log

where the python option ``LogY``, if set to true, allows to plot data/MC with log scale on the y axis.
