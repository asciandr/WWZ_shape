//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Aug 26 14:19:54 2018 by ROOT version 5.34/38
// from TTree nominal/nominal
// found on file: /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/363507.root
//////////////////////////////////////////////////////////

#ifndef nominal_h
#define nominal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class nominal {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          RunNumber;
   UInt_t          RunYear;
   UInt_t          MCchannelNumber;
   Float_t         mcWeightOrg;
   vector<float>   *AllmcWeightOrg;
   Float_t         xsec;
   Float_t         totweight;
   ULong64_t       tot_events;
   Float_t         average_mu;
   Float_t         pileupEventWeight;
   Float_t         pileupEventWeight_UP;
   Float_t         pileupEventWeight_DOWN;
   Float_t         JVT_EventWeight;
   Float_t         JVT_EventWeight_UP;
   Float_t         JVT_EventWeight_DOWN;
   Float_t         MV2c10_FixedCutBEff_70_EventWeight;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_B_up;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_B_down;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_C_up;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_C_down;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up;
   vector<float>   *bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down;
   ULong64_t       EventNumber;
   Bool_t          passTrigger;
   Bool_t          passEventCleaning;
   Bool_t          isTrigMatched;
   Int_t           total_charge;
   Float_t         Mllll0123;
   Float_t         Mlll012;
   Float_t         Mlll013;
   Float_t         Mlll023;
   Float_t         Mlll123;
   Float_t         Mll01;
   Float_t         Mll02;
   Float_t         Mll03;
   Float_t         Mll12;
   Float_t         Mll13;
   Float_t         Mll23;
   Float_t         best_Z_Mll;
   Float_t         best_Z_other_Mll;
   Float_t         Mjj01;
   Float_t         Mjj02;
   Float_t         Mjj03;
   Float_t         Mjj04;
   Float_t         Mjj05;
   Float_t         Mjj12;
   Float_t         Mjj13;
   Float_t         Mjj14;
   Float_t         Mjj15;
   Float_t         Mjj23;
   Float_t         Mjj24;
   Float_t         Mjj25;
   Float_t         Mjj34;
   Float_t         Mjj35;
   Float_t         Mjj45;
   Float_t         HT_lep;
   Float_t         HT_had;
   Float_t         MET_RefFinal_et;
   Float_t         MET_RefFinal_phi;
   Float_t         MET_RefFinal_x;
   Float_t         MET_RefFinal_y;
   vector<float>   *jet_E;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_jvt;
   vector<int>     *jet_coneLabel;
   vector<double>  *jet_mv2c10;
   Int_t           nJets_mv2c10_60;
   Int_t           nJets_mv2c10_70;
   Int_t           nJets_mv2c10_77;
   Int_t           nJets_mv2c10_85;
   vector<float>   *lepton_ID;
   vector<float>   *lepton_E;
   vector<float>   *lepton_pt;
   vector<float>   *lepton_eta;
   vector<float>   *lepton_phi;
   vector<float>   *lepton_q;
   vector<float>   *lepton_d0;
   vector<float>   *lepton_z0;
   vector<float>   *lepton_z0sinT;
   vector<int>     *lepton_truthType;
   vector<int>     *lepton_truthOrigin;
   vector<double>  *lepton_sigd0;
   vector<double>  *lepton_PromptLeptonVeto;
   vector<double>  *lepton_PromptLeptonIso_TagWeight;
   vector<bool>    *lepton_isTightLH;
   vector<bool>    *lepton_isTightID;
   vector<float>   *lepton_chargeIDBDTTight;
   vector<unsigned char> *lepton_ambiguityType;
   vector<short>   *lepton_isolationFixedCutLoose;
   vector<float>   *lepton_SFIDLoose;
   vector<float>   *lepton_SFIDTightLH;
   vector<float>   *lepton_SFIsoLoose;
   vector<float>   *lepton_SFIsoPLV;
   vector<float>   *lepton_SFReco;
   vector<float>   *lepton_SFTTVA;
   vector<float>   *lepton_SFObjLoose;
   vector<float>   *lepton_SFObjLoose_EL_SF_ID_UP;
   vector<float>   *lepton_SFObjLoose_EL_SF_ID_DOWN;
   vector<float>   *lepton_SFObjLoose_EL_SF_Reco_UP;
   vector<float>   *lepton_SFObjLoose_EL_SF_Reco_DOWN;
   vector<float>   *lepton_SFObjLoose_EL_SF_Iso_UP;
   vector<float>   *lepton_SFObjLoose_EL_SF_Iso_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_RECO_STAT_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_RECO_SYST_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_Iso_STAT_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_Iso_SYST_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_TTVA_STAT_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN;
   vector<float>   *lepton_SFObjLoose_MU_SF_TTVA_SYST_UP;
   vector<float>   *lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN;
   vector<float>   *lepton_SFObjTightLH;
   vector<float>   *lepton_SFObjTightLH_EL_SF_ID_UP;
   vector<float>   *lepton_SFObjTightLH_EL_SF_ID_DOWN;
   vector<float>   *lepton_SFObjTightLH_EL_SF_Reco_UP;
   vector<float>   *lepton_SFObjTightLH_EL_SF_Reco_DOWN;
   vector<float>   *lepton_SFObjTightLH_EL_SF_Iso_UP;
   vector<float>   *lepton_SFObjTightLH_EL_SF_Iso_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_RECO_STAT_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_RECO_STAT_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_RECO_SYST_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_RECO_SYST_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_Iso_STAT_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_Iso_STAT_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_Iso_SYST_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_Iso_SYST_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_TTVA_STAT_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN;
   vector<float>   *lepton_SFObjTight_MU_SF_TTVA_SYST_UP;
   vector<float>   *lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN;
   vector<float>   *muon_E;
   vector<float>   *muon_pt;
   vector<float>   *muon_eta;
   vector<float>   *muon_phi;
   vector<float>   *muon_q;
   vector<float>   *muon_d0;
   vector<float>   *muon_z0;
   vector<float>   *muon_z0sinT;
   vector<int>     *muon_truthType;
   vector<int>     *muon_truthOrigin;
   vector<double>  *muon_sigd0;
   vector<float>   *electron_E;
   vector<float>   *electron_pt;
   vector<float>   *electron_eta;
   vector<float>   *electron_phi;
   vector<float>   *electron_q;
   vector<float>   *electron_d0;
   vector<float>   *electron_z0;
   vector<float>   *electron_z0sinT;
   vector<int>     *electron_truthType;
   vector<int>     *electron_truthOrigin;
   vector<double>  *electron_sigd0;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RunYear;   //!
   TBranch        *b_MCchannelNumber;   //!
   TBranch        *b_mcWeightOrg;   //!
   TBranch        *b_AllmcWeightOrg;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_totweight;   //!
   TBranch        *b_tot_events;   //!
   TBranch        *b_average_mu;   //!
   TBranch        *b_pileupEventWeight;   //!
   TBranch        *b_pileupEventWeight_UP;   //!
   TBranch        *b_pileupEventWeight_DOWN;   //!
   TBranch        *b_JVT_EventWeight;   //!
   TBranch        *b_JVT_EventWeight_UP;   //!
   TBranch        *b_JVT_EventWeight_DOWN;   //!
   TBranch        *b_MV2c10_FixedCutBEff_70_EventWeight;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up;   //!
   TBranch        *b_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_passTrigger;   //!
   TBranch        *b_passEventCleaning;   //!
   TBranch        *b_isTrigMatched;   //!
   TBranch        *b_total_charge;   //!
   TBranch        *b_Mllll0123;   //!
   TBranch        *b_Mlll012;   //!
   TBranch        *b_Mlll013;   //!
   TBranch        *b_Mlll023;   //!
   TBranch        *b_Mlll123;   //!
   TBranch        *b_Mll01;   //!
   TBranch        *b_Mll02;   //!
   TBranch        *b_Mll03;   //!
   TBranch        *b_Mll12;   //!
   TBranch        *b_Mll13;   //!
   TBranch        *b_Mll23;   //!
   TBranch        *b_best_Z_Mll;   //!
   TBranch        *b_best_Z_other_Mll;   //!
   TBranch        *b_Mjj01;   //!
   TBranch        *b_Mjj02;   //!
   TBranch        *b_Mjj03;   //!
   TBranch        *b_Mjj04;   //!
   TBranch        *b_Mjj05;   //!
   TBranch        *b_Mjj12;   //!
   TBranch        *b_Mjj13;   //!
   TBranch        *b_Mjj14;   //!
   TBranch        *b_Mjj15;   //!
   TBranch        *b_Mjj23;   //!
   TBranch        *b_Mjj24;   //!
   TBranch        *b_Mjj25;   //!
   TBranch        *b_Mjj34;   //!
   TBranch        *b_Mjj35;   //!
   TBranch        *b_Mjj45;   //!
   TBranch        *b_HT_lep;   //!
   TBranch        *b_HT_had;   //!
   TBranch        *b_MET_RefFinal_et;   //!
   TBranch        *b_MET_RefFinal_phi;   //!
   TBranch        *b_MET_RefFinal_x;   //!
   TBranch        *b_MET_RefFinal_y;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_coneLabel;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_nJets_mv2c10_60;   //!
   TBranch        *b_nJets_mv2c10_70;   //!
   TBranch        *b_nJets_mv2c10_77;   //!
   TBranch        *b_nJets_mv2c10_85;   //!
   TBranch        *b_lepton_ID;   //!
   TBranch        *b_lepton_E;   //!
   TBranch        *b_lepton_pt;   //!
   TBranch        *b_lepton_eta;   //!
   TBranch        *b_lepton_phi;   //!
   TBranch        *b_lepton_q;   //!
   TBranch        *b_lepton_d0;   //!
   TBranch        *b_lepton_z0;   //!
   TBranch        *b_lepton_z0sinT;   //!
   TBranch        *b_lepton_truthType;   //!
   TBranch        *b_lepton_truthOrigin;   //!
   TBranch        *b_lepton_sigd0;   //!
   TBranch        *b_lepton_PromptLeptonVeto;   //!
   TBranch        *b_lepton_PromptLeptonIso_TagWeight;   //!
   TBranch        *b_lepton_isTightLH;   //!
   TBranch        *b_lepton_isTightID;   //!
   TBranch        *b_lepton_chargeIDBDTTight;   //!
   TBranch        *b_lepton_ambiguityType;   //!
   TBranch        *b_lepton_isolationFixedCutLoose;   //!
   TBranch        *b_lepton_SFIDLoose;   //!
   TBranch        *b_lepton_SFIDTightLH;   //!
   TBranch        *b_lepton_SFIsoLoose;   //!
   TBranch        *b_lepton_SFIsoPLV;   //!
   TBranch        *b_lepton_SFReco;   //!
   TBranch        *b_lepton_SFTTVA;   //!
   TBranch        *b_lepton_SFObjLoose;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_ID_UP;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_ID_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_Reco_UP;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_Reco_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_Iso_UP;   //!
   TBranch        *b_lepton_SFObjLoose_EL_SF_Iso_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_RECO_STAT_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_RECO_SYST_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_Iso_STAT_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_Iso_SYST_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_lepton_SFObjTightLH;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_ID_UP;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_ID_DOWN;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_Reco_UP;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_Reco_DOWN;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_Iso_UP;   //!
   TBranch        *b_lepton_SFObjTightLH_EL_SF_Iso_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_RECO_STAT_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_RECO_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_RECO_SYST_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_RECO_SYST_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_Iso_STAT_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_Iso_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_Iso_SYST_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_Iso_SYST_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_muon_E;   //!
   TBranch        *b_muon_pt;   //!
   TBranch        *b_muon_eta;   //!
   TBranch        *b_muon_phi;   //!
   TBranch        *b_muon_q;   //!
   TBranch        *b_muon_d0;   //!
   TBranch        *b_muon_z0;   //!
   TBranch        *b_muon_z0sinT;   //!
   TBranch        *b_muon_truthType;   //!
   TBranch        *b_muon_truthOrigin;   //!
   TBranch        *b_muon_sigd0;   //!
   TBranch        *b_electron_E;   //!
   TBranch        *b_electron_pt;   //!
   TBranch        *b_electron_eta;   //!
   TBranch        *b_electron_phi;   //!
   TBranch        *b_electron_q;   //!
   TBranch        *b_electron_d0;   //!
   TBranch        *b_electron_z0;   //!
   TBranch        *b_electron_z0sinT;   //!
   TBranch        *b_electron_truthType;   //!
   TBranch        *b_electron_truthOrigin;   //!
   TBranch        *b_electron_sigd0;   //!

   nominal(TTree *tree=0);
   virtual ~nominal();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef nominal_cxx
nominal::nominal(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/363507.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/363507.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

nominal::~nominal()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t nominal::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t nominal::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   AllmcWeightOrg = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_B_up = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_B_down = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_C_up = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_C_down = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up = 0;
   bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down = 0;
   jet_E = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_jvt = 0;
   jet_coneLabel = 0;
   jet_mv2c10 = 0;
   lepton_ID = 0;
   lepton_E = 0;
   lepton_pt = 0;
   lepton_eta = 0;
   lepton_phi = 0;
   lepton_q = 0;
   lepton_d0 = 0;
   lepton_z0 = 0;
   lepton_z0sinT = 0;
   lepton_truthType = 0;
   lepton_truthOrigin = 0;
   lepton_sigd0 = 0;
   lepton_PromptLeptonVeto = 0;
   lepton_PromptLeptonIso_TagWeight = 0;
   lepton_isTightLH = 0;
   lepton_isTightID = 0;
   lepton_chargeIDBDTTight = 0;
   lepton_ambiguityType = 0;
   lepton_isolationFixedCutLoose = 0;
   lepton_SFIDLoose = 0;
   lepton_SFIDTightLH = 0;
   lepton_SFIsoLoose = 0;
   lepton_SFIsoPLV = 0;
   lepton_SFReco = 0;
   lepton_SFTTVA = 0;
   lepton_SFObjLoose = 0;
   lepton_SFObjLoose_EL_SF_ID_UP = 0;
   lepton_SFObjLoose_EL_SF_ID_DOWN = 0;
   lepton_SFObjLoose_EL_SF_Reco_UP = 0;
   lepton_SFObjLoose_EL_SF_Reco_DOWN = 0;
   lepton_SFObjLoose_EL_SF_Iso_UP = 0;
   lepton_SFObjLoose_EL_SF_Iso_DOWN = 0;
   lepton_SFObjLoose_MU_SF_RECO_STAT_UP = 0;
   lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN = 0;
   lepton_SFObjLoose_MU_SF_RECO_SYST_UP = 0;
   lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN = 0;
   lepton_SFObjLoose_MU_SF_Iso_STAT_UP = 0;
   lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN = 0;
   lepton_SFObjLoose_MU_SF_Iso_SYST_UP = 0;
   lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN = 0;
   lepton_SFObjLoose_MU_SF_TTVA_STAT_UP = 0;
   lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN = 0;
   lepton_SFObjLoose_MU_SF_TTVA_SYST_UP = 0;
   lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN = 0;
   lepton_SFObjTightLH = 0;
   lepton_SFObjTightLH_EL_SF_ID_UP = 0;
   lepton_SFObjTightLH_EL_SF_ID_DOWN = 0;
   lepton_SFObjTightLH_EL_SF_Reco_UP = 0;
   lepton_SFObjTightLH_EL_SF_Reco_DOWN = 0;
   lepton_SFObjTightLH_EL_SF_Iso_UP = 0;
   lepton_SFObjTightLH_EL_SF_Iso_DOWN = 0;
   lepton_SFObjTight_MU_SF_RECO_STAT_UP = 0;
   lepton_SFObjTight_MU_SF_RECO_STAT_DOWN = 0;
   lepton_SFObjTight_MU_SF_RECO_SYST_UP = 0;
   lepton_SFObjTight_MU_SF_RECO_SYST_DOWN = 0;
   lepton_SFObjTight_MU_SF_Iso_STAT_UP = 0;
   lepton_SFObjTight_MU_SF_Iso_STAT_DOWN = 0;
   lepton_SFObjTight_MU_SF_Iso_SYST_UP = 0;
   lepton_SFObjTight_MU_SF_Iso_SYST_DOWN = 0;
   lepton_SFObjTight_MU_SF_TTVA_STAT_UP = 0;
   lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN = 0;
   lepton_SFObjTight_MU_SF_TTVA_SYST_UP = 0;
   lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN = 0;
   muon_E = 0;
   muon_pt = 0;
   muon_eta = 0;
   muon_phi = 0;
   muon_q = 0;
   muon_d0 = 0;
   muon_z0 = 0;
   muon_z0sinT = 0;
   muon_truthType = 0;
   muon_truthOrigin = 0;
   muon_sigd0 = 0;
   electron_E = 0;
   electron_pt = 0;
   electron_eta = 0;
   electron_phi = 0;
   electron_q = 0;
   electron_d0 = 0;
   electron_z0 = 0;
   electron_z0sinT = 0;
   electron_truthType = 0;
   electron_truthOrigin = 0;
   electron_sigd0 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("RunYear", &RunYear, &b_RunYear);
   fChain->SetBranchAddress("MCchannelNumber", &MCchannelNumber, &b_MCchannelNumber);
   fChain->SetBranchAddress("mcWeightOrg", &mcWeightOrg, &b_mcWeightOrg);
   fChain->SetBranchAddress("AllmcWeightOrg", &AllmcWeightOrg, &b_AllmcWeightOrg);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("totweight", &totweight, &b_totweight);
   fChain->SetBranchAddress("tot_events", &tot_events, &b_tot_events);
   fChain->SetBranchAddress("average_mu", &average_mu, &b_average_mu);
   fChain->SetBranchAddress("pileupEventWeight", &pileupEventWeight, &b_pileupEventWeight);
   fChain->SetBranchAddress("pileupEventWeight_UP", &pileupEventWeight_UP, &b_pileupEventWeight_UP);
   fChain->SetBranchAddress("pileupEventWeight_DOWN", &pileupEventWeight_DOWN, &b_pileupEventWeight_DOWN);
   fChain->SetBranchAddress("JVT_EventWeight", &JVT_EventWeight, &b_JVT_EventWeight);
   fChain->SetBranchAddress("JVT_EventWeight_UP", &JVT_EventWeight_UP, &b_JVT_EventWeight_UP);
   fChain->SetBranchAddress("JVT_EventWeight_DOWN", &JVT_EventWeight_DOWN, &b_JVT_EventWeight_DOWN);
   fChain->SetBranchAddress("MV2c10_FixedCutBEff_70_EventWeight", &MV2c10_FixedCutBEff_70_EventWeight, &b_MV2c10_FixedCutBEff_70_EventWeight);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_B_up", &bTagSF_weight_MV2c10_FixedCutBEff_70_B_up, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_B_up);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_B_down", &bTagSF_weight_MV2c10_FixedCutBEff_70_B_down, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_B_down);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_C_up", &bTagSF_weight_MV2c10_FixedCutBEff_70_C_up, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_C_up);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_C_down", &bTagSF_weight_MV2c10_FixedCutBEff_70_C_down, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_C_down);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up", &bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_up);
   fChain->SetBranchAddress("bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down", &bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down, &b_bTagSF_weight_MV2c10_FixedCutBEff_70_Light_down);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("passTrigger", &passTrigger, &b_passTrigger);
   fChain->SetBranchAddress("passEventCleaning", &passEventCleaning, &b_passEventCleaning);
   fChain->SetBranchAddress("isTrigMatched", &isTrigMatched, &b_isTrigMatched);
   fChain->SetBranchAddress("total_charge", &total_charge, &b_total_charge);
   fChain->SetBranchAddress("Mllll0123", &Mllll0123, &b_Mllll0123);
   fChain->SetBranchAddress("Mlll012", &Mlll012, &b_Mlll012);
   fChain->SetBranchAddress("Mlll013", &Mlll013, &b_Mlll013);
   fChain->SetBranchAddress("Mlll023", &Mlll023, &b_Mlll023);
   fChain->SetBranchAddress("Mlll123", &Mlll123, &b_Mlll123);
   fChain->SetBranchAddress("Mll01", &Mll01, &b_Mll01);
   fChain->SetBranchAddress("Mll02", &Mll02, &b_Mll02);
   fChain->SetBranchAddress("Mll03", &Mll03, &b_Mll03);
   fChain->SetBranchAddress("Mll12", &Mll12, &b_Mll12);
   fChain->SetBranchAddress("Mll13", &Mll13, &b_Mll13);
   fChain->SetBranchAddress("Mll23", &Mll23, &b_Mll23);
   fChain->SetBranchAddress("best_Z_Mll", &best_Z_Mll, &b_best_Z_Mll);
   fChain->SetBranchAddress("best_Z_other_Mll", &best_Z_other_Mll, &b_best_Z_other_Mll);
   fChain->SetBranchAddress("Mjj01", &Mjj01, &b_Mjj01);
   fChain->SetBranchAddress("Mjj02", &Mjj02, &b_Mjj02);
   fChain->SetBranchAddress("Mjj03", &Mjj03, &b_Mjj03);
   fChain->SetBranchAddress("Mjj04", &Mjj04, &b_Mjj04);
   fChain->SetBranchAddress("Mjj05", &Mjj05, &b_Mjj05);
   fChain->SetBranchAddress("Mjj12", &Mjj12, &b_Mjj12);
   fChain->SetBranchAddress("Mjj13", &Mjj13, &b_Mjj13);
   fChain->SetBranchAddress("Mjj14", &Mjj14, &b_Mjj14);
   fChain->SetBranchAddress("Mjj15", &Mjj15, &b_Mjj15);
   fChain->SetBranchAddress("Mjj23", &Mjj23, &b_Mjj23);
   fChain->SetBranchAddress("Mjj24", &Mjj24, &b_Mjj24);
   fChain->SetBranchAddress("Mjj25", &Mjj25, &b_Mjj25);
   fChain->SetBranchAddress("Mjj34", &Mjj34, &b_Mjj34);
   fChain->SetBranchAddress("Mjj35", &Mjj35, &b_Mjj35);
   fChain->SetBranchAddress("Mjj45", &Mjj45, &b_Mjj45);
   fChain->SetBranchAddress("HT_lep", &HT_lep, &b_HT_lep);
   fChain->SetBranchAddress("HT_had", &HT_had, &b_HT_had);
   fChain->SetBranchAddress("MET_RefFinal_et", &MET_RefFinal_et, &b_MET_RefFinal_et);
   fChain->SetBranchAddress("MET_RefFinal_phi", &MET_RefFinal_phi, &b_MET_RefFinal_phi);
   fChain->SetBranchAddress("MET_RefFinal_x", &MET_RefFinal_x, &b_MET_RefFinal_x);
   fChain->SetBranchAddress("MET_RefFinal_y", &MET_RefFinal_y, &b_MET_RefFinal_y);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_coneLabel", &jet_coneLabel, &b_jet_coneLabel);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("nJets_mv2c10_60", &nJets_mv2c10_60, &b_nJets_mv2c10_60);
   fChain->SetBranchAddress("nJets_mv2c10_70", &nJets_mv2c10_70, &b_nJets_mv2c10_70);
   fChain->SetBranchAddress("nJets_mv2c10_77", &nJets_mv2c10_77, &b_nJets_mv2c10_77);
   fChain->SetBranchAddress("nJets_mv2c10_85", &nJets_mv2c10_85, &b_nJets_mv2c10_85);
   fChain->SetBranchAddress("lepton_ID", &lepton_ID, &b_lepton_ID);
   fChain->SetBranchAddress("lepton_E", &lepton_E, &b_lepton_E);
   fChain->SetBranchAddress("lepton_pt", &lepton_pt, &b_lepton_pt);
   fChain->SetBranchAddress("lepton_eta", &lepton_eta, &b_lepton_eta);
   fChain->SetBranchAddress("lepton_phi", &lepton_phi, &b_lepton_phi);
   fChain->SetBranchAddress("lepton_q", &lepton_q, &b_lepton_q);
   fChain->SetBranchAddress("lepton_d0", &lepton_d0, &b_lepton_d0);
   fChain->SetBranchAddress("lepton_z0", &lepton_z0, &b_lepton_z0);
   fChain->SetBranchAddress("lepton_z0sinT", &lepton_z0sinT, &b_lepton_z0sinT);
   fChain->SetBranchAddress("lepton_truthType", &lepton_truthType, &b_lepton_truthType);
   fChain->SetBranchAddress("lepton_truthOrigin", &lepton_truthOrigin, &b_lepton_truthOrigin);
   fChain->SetBranchAddress("lepton_sigd0", &lepton_sigd0, &b_lepton_sigd0);
   fChain->SetBranchAddress("lepton_PromptLeptonVeto", &lepton_PromptLeptonVeto, &b_lepton_PromptLeptonVeto);
   fChain->SetBranchAddress("lepton_PromptLeptonIso_TagWeight", &lepton_PromptLeptonIso_TagWeight, &b_lepton_PromptLeptonIso_TagWeight);
   fChain->SetBranchAddress("lepton_isTightLH", &lepton_isTightLH, &b_lepton_isTightLH);
   fChain->SetBranchAddress("lepton_isTightID", &lepton_isTightID, &b_lepton_isTightID);
   fChain->SetBranchAddress("lepton_chargeIDBDTTight", &lepton_chargeIDBDTTight, &b_lepton_chargeIDBDTTight);
   fChain->SetBranchAddress("lepton_ambiguityType", &lepton_ambiguityType, &b_lepton_ambiguityType);
   fChain->SetBranchAddress("lepton_isolationFixedCutLoose", &lepton_isolationFixedCutLoose, &b_lepton_isolationFixedCutLoose);
   fChain->SetBranchAddress("lepton_SFIDLoose", &lepton_SFIDLoose, &b_lepton_SFIDLoose);
   fChain->SetBranchAddress("lepton_SFIDTightLH", &lepton_SFIDTightLH, &b_lepton_SFIDTightLH);
   fChain->SetBranchAddress("lepton_SFIsoLoose", &lepton_SFIsoLoose, &b_lepton_SFIsoLoose);
   fChain->SetBranchAddress("lepton_SFIsoPLV", &lepton_SFIsoPLV, &b_lepton_SFIsoPLV);
   fChain->SetBranchAddress("lepton_SFReco", &lepton_SFReco, &b_lepton_SFReco);
   fChain->SetBranchAddress("lepton_SFTTVA", &lepton_SFTTVA, &b_lepton_SFTTVA);
   fChain->SetBranchAddress("lepton_SFObjLoose", &lepton_SFObjLoose, &b_lepton_SFObjLoose);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_ID_UP", &lepton_SFObjLoose_EL_SF_ID_UP, &b_lepton_SFObjLoose_EL_SF_ID_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_ID_DOWN", &lepton_SFObjLoose_EL_SF_ID_DOWN, &b_lepton_SFObjLoose_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_Reco_UP", &lepton_SFObjLoose_EL_SF_Reco_UP, &b_lepton_SFObjLoose_EL_SF_Reco_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_Reco_DOWN", &lepton_SFObjLoose_EL_SF_Reco_DOWN, &b_lepton_SFObjLoose_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_Iso_UP", &lepton_SFObjLoose_EL_SF_Iso_UP, &b_lepton_SFObjLoose_EL_SF_Iso_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_EL_SF_Iso_DOWN", &lepton_SFObjLoose_EL_SF_Iso_DOWN, &b_lepton_SFObjLoose_EL_SF_Iso_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_RECO_STAT_UP", &lepton_SFObjLoose_MU_SF_RECO_STAT_UP, &b_lepton_SFObjLoose_MU_SF_RECO_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN", &lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN, &b_lepton_SFObjLoose_MU_SF_RECO_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_RECO_SYST_UP", &lepton_SFObjLoose_MU_SF_RECO_SYST_UP, &b_lepton_SFObjLoose_MU_SF_RECO_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN", &lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN, &b_lepton_SFObjLoose_MU_SF_RECO_SYST_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_Iso_STAT_UP", &lepton_SFObjLoose_MU_SF_Iso_STAT_UP, &b_lepton_SFObjLoose_MU_SF_Iso_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN", &lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN, &b_lepton_SFObjLoose_MU_SF_Iso_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_Iso_SYST_UP", &lepton_SFObjLoose_MU_SF_Iso_SYST_UP, &b_lepton_SFObjLoose_MU_SF_Iso_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN", &lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN, &b_lepton_SFObjLoose_MU_SF_Iso_SYST_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_TTVA_STAT_UP", &lepton_SFObjLoose_MU_SF_TTVA_STAT_UP, &b_lepton_SFObjLoose_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN", &lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN, &b_lepton_SFObjLoose_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_TTVA_SYST_UP", &lepton_SFObjLoose_MU_SF_TTVA_SYST_UP, &b_lepton_SFObjLoose_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN", &lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN, &b_lepton_SFObjLoose_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTightLH", &lepton_SFObjTightLH, &b_lepton_SFObjTightLH);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_ID_UP", &lepton_SFObjTightLH_EL_SF_ID_UP, &b_lepton_SFObjTightLH_EL_SF_ID_UP);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_ID_DOWN", &lepton_SFObjTightLH_EL_SF_ID_DOWN, &b_lepton_SFObjTightLH_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_Reco_UP", &lepton_SFObjTightLH_EL_SF_Reco_UP, &b_lepton_SFObjTightLH_EL_SF_Reco_UP);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_Reco_DOWN", &lepton_SFObjTightLH_EL_SF_Reco_DOWN, &b_lepton_SFObjTightLH_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_Iso_UP", &lepton_SFObjTightLH_EL_SF_Iso_UP, &b_lepton_SFObjTightLH_EL_SF_Iso_UP);
   fChain->SetBranchAddress("lepton_SFObjTightLH_EL_SF_Iso_DOWN", &lepton_SFObjTightLH_EL_SF_Iso_DOWN, &b_lepton_SFObjTightLH_EL_SF_Iso_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_RECO_STAT_UP", &lepton_SFObjTight_MU_SF_RECO_STAT_UP, &b_lepton_SFObjTight_MU_SF_RECO_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_RECO_STAT_DOWN", &lepton_SFObjTight_MU_SF_RECO_STAT_DOWN, &b_lepton_SFObjTight_MU_SF_RECO_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_RECO_SYST_UP", &lepton_SFObjTight_MU_SF_RECO_SYST_UP, &b_lepton_SFObjTight_MU_SF_RECO_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_RECO_SYST_DOWN", &lepton_SFObjTight_MU_SF_RECO_SYST_DOWN, &b_lepton_SFObjTight_MU_SF_RECO_SYST_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_Iso_STAT_UP", &lepton_SFObjTight_MU_SF_Iso_STAT_UP, &b_lepton_SFObjTight_MU_SF_Iso_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_Iso_STAT_DOWN", &lepton_SFObjTight_MU_SF_Iso_STAT_DOWN, &b_lepton_SFObjTight_MU_SF_Iso_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_Iso_SYST_UP", &lepton_SFObjTight_MU_SF_Iso_SYST_UP, &b_lepton_SFObjTight_MU_SF_Iso_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_Iso_SYST_DOWN", &lepton_SFObjTight_MU_SF_Iso_SYST_DOWN, &b_lepton_SFObjTight_MU_SF_Iso_SYST_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_TTVA_STAT_UP", &lepton_SFObjTight_MU_SF_TTVA_STAT_UP, &b_lepton_SFObjTight_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN", &lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN, &b_lepton_SFObjTight_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_TTVA_SYST_UP", &lepton_SFObjTight_MU_SF_TTVA_SYST_UP, &b_lepton_SFObjTight_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN", &lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN, &b_lepton_SFObjTight_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("muon_E", &muon_E, &b_muon_E);
   fChain->SetBranchAddress("muon_pt", &muon_pt, &b_muon_pt);
   fChain->SetBranchAddress("muon_eta", &muon_eta, &b_muon_eta);
   fChain->SetBranchAddress("muon_phi", &muon_phi, &b_muon_phi);
   fChain->SetBranchAddress("muon_q", &muon_q, &b_muon_q);
   fChain->SetBranchAddress("muon_d0", &muon_d0, &b_muon_d0);
   fChain->SetBranchAddress("muon_z0", &muon_z0, &b_muon_z0);
   fChain->SetBranchAddress("muon_z0sinT", &muon_z0sinT, &b_muon_z0sinT);
   fChain->SetBranchAddress("muon_truthType", &muon_truthType, &b_muon_truthType);
   fChain->SetBranchAddress("muon_truthOrigin", &muon_truthOrigin, &b_muon_truthOrigin);
   fChain->SetBranchAddress("muon_sigd0", &muon_sigd0, &b_muon_sigd0);
   fChain->SetBranchAddress("electron_E", &electron_E, &b_electron_E);
   fChain->SetBranchAddress("electron_pt", &electron_pt, &b_electron_pt);
   fChain->SetBranchAddress("electron_eta", &electron_eta, &b_electron_eta);
   fChain->SetBranchAddress("electron_phi", &electron_phi, &b_electron_phi);
   fChain->SetBranchAddress("electron_q", &electron_q, &b_electron_q);
   fChain->SetBranchAddress("electron_d0", &electron_d0, &b_electron_d0);
   fChain->SetBranchAddress("electron_z0", &electron_z0, &b_electron_z0);
   fChain->SetBranchAddress("electron_z0sinT", &electron_z0sinT, &b_electron_z0sinT);
   fChain->SetBranchAddress("electron_truthType", &electron_truthType, &b_electron_truthType);
   fChain->SetBranchAddress("electron_truthOrigin", &electron_truthOrigin, &b_electron_truthOrigin);
   fChain->SetBranchAddress("electron_sigd0", &electron_sigd0, &b_electron_sigd0);
   Notify();
}

Bool_t nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void nominal::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t nominal::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef nominal_cxx
