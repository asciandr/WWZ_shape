#!/bin/bash

pwd
workdir=`pwd`
echo XSAMPTXT
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT --skipConfirm
cp -rf /afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/Skim/{skimNtuples.C,selection.txt,outFolder.txt,nominal.h,nominal.C,run.py} .
ls -haltr
### IF ON EOSPUBLIC
#source /afs/cern.ch/project/eos/installation/atlas/etc/setup.sh  # setup eos
#export EOS_MGM_URL=root://eospublic.cern.ch/
#eosmount ./eospublic
#ls -haltr eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v29/01/gfw2/2017-06-22/Data/XFILE
#python run.py ./eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v29/01/gfw2/2017-06-22/Data/XFILE selection.txt outFolder.txt
# --- v1.0 --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v1.0/MC16c/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v1.0/MC16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v1.0/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v1.0/MC16a/XFILE selection.txt outFolder.txt
# --- v2.0 --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v2.0/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v2.0/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v2.0/data2016/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v2.0/data2016/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v2.0/MC16c/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v2.0/MC16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/wFlatBranches_skimmed_v2.0/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/wFlatBranches_skimmed_v2.0/XFILE selection.txt outFolder.txt
# --- v3.0 --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v3.0/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v3.0/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v3.0/data_hadded/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v3.0/data_hadded/XFILE selection.txt outFolder.txt
# --- v3.1 --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v3.1/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v3.1/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v3.1/MC16c/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v3.1/MC16c/XFILE selection.txt outFolder.txt
# --- v5.0 --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16c/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16d/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v5.0/MC16d/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v5.0/data/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v5.0/data/XFILE selection.txt outFolder.txt
# --- v5.0 skim for BDT trainings --- #
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/hadded_TightLHSF_SYST_MC16_wFlatBranches_skimmed_v5.0/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/hadded_TightLHSF_SYST_MC16_wFlatBranches_skimmed_v5.0/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v6/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v6/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v6/MC16d/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v6/MC16d/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/v6/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/v6/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/full_syst/MC16d/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/full_syst/MC16d/XFILE selection.txt outFolder.txt
### FULL-SYST v7 production
ls -haltr /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16a/XFILE
python run.py /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16a/XFILE selection.txt outFolder.txt XTTREE
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-sm/WVZ_Run2/v7_Sys/MC16d/XFILE selection.txt outFolder.txt XTTREE
### NOMINAL v7 production
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16a/XFILE selection.txt outFolder.txt XTTREE
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16d/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/Nomv7/MC16d/XFILE selection.txt outFolder.txt XTTREE
### DATA v7 production
#ls -haltr /eos/atlas/user/a/asciandr/WWZ/Nomv7/data/XFILE
#python run.py /eos/atlas/user/a/asciandr/WWZ/Nomv7/data/XFILE selection.txt outFolder.txt XTTREE
echo 'done.'
