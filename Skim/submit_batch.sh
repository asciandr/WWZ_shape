#!/bin/bash

listtxt=$1
listttrees=$2
while read samp
do
    echo $samp
    samptxt=`echo $samp| sed "s/.root/.txt/g" `
    echo $samptxt
    sampname=`echo $samp| sed "s/.root//g" `
    echo $sampname
    echo $samp > $samptxt
    while read ttreename
    do
        echo $ttreename
        cat job.sh | sed "s/XTTREE/$ttreename/g" | sed "s/XSAMPTXT/$samptxt/g" | sed "s/XFILE/$samp/g" > job_${sampname}_${ttreename}.sh
        chmod 755 job_${sampname}_${ttreename}.sh
        echo job_${sampname}_${ttreename}.sh
        #queue="1nh"
        #queue="8nh"
        #queue="1nd"
        queue="2nd"
        bsub -q $queue job_${sampname}_${ttreename}.sh
        echo $queue
    done < $2
done < $1
