# remove hadded files
#rm -v WWZ.root WZZ.root WVZ.root

####---- NOMINAL SAMPLES ----####
# WVZ
hadd -k WWZ.root                363507.root 364243.root 364244.root
hadd -k WZZ.root                363508.root 363509.root 364246.root 364245.root
# total signal
hadd -k WVZ.root		WWZ.root WZZ.root

# VV
# llll
hadd -k llll.root		364250.root
hadd -k gg_llll.root		345705.root 345706.root
hadd -k lowllll.root		364288.root
# ZZ, N.B. lllljj (VBS) apart
hadd -k ZZ.root			llll.root gg_llll.root lowllll.root

# lllv
hadd -k lllv.root		364253.root
hadd -k lowlllv.root		364289.root
# WZ, N.B. lllvjj (VBS) apart
hadd -k WZ.root			lllv.root lowlllv.root

# lllljj and lllvjj VBS
hadd -k lllljj.root		364283.root
hadd -k lllvjj.root		364284.root

# Z+j
hadd -k Zjets.root              3641*.root

# Z+gamma
hadd -k Zgamma.root             36450*.root

# ttZ
hadd -k ttZ.root                410218.root 410219.root 410220.root     # aMC@NLO  + Pythia8

# tZ
hadd -k tZ.root                 410560.root  # nonallhad 

# tWZ
hadd -k tWZ.root                410217.root

# VH
hadd -k VH.root                 342284.root 342285.root

# OTHERS
# ttH
hadd -k ttH.root   		343365.root #343366.root 343367.root				# aMC@NLO + Pythia8
# ttW
hadd -k ttW.root   		410155.root
hadd -k WW.root 		364254.root
hadd -k lowWW.root		364290.root
hadd -k ZqqZll.root		363356.root
hadd -k WqqZll.root		363358.root
hadd -k llvvjj.root		364285.root
hadd -k llvvjj_ss.root		364287.root
# ttbar
hadd -k ttbar_nonallhad.root	410501.root
hadd -k ttbar_2lep_PP8.root 	410503.root    # Powheg + Pythia8 dilept filter
hadd -k ttgamma.root		410389.root
# single top
hadd -k singletop.root      	4100{11..14}.root 410025.root 410026.root  # is it ok to use 13,14 (which are Wt inclusive)   and not 15,16 (Wt dilept) also ;)
# WWW
hadd -k WWW.root             	364242.root
# ZZZ
hadd -k ZZZ.root             	364247.root 364248.root 364249.root
# tWH
hadd -k tWH.root             	341998.root 342001.root 342004.root
# tHjb
hadd -k tHjb.root          	343267.root 343270.root 343273.root
# tttt
hadd -k tttt.root		410080.root
# ttWW
hadd -k ttWW.root 		410081.root
# hadd OTHERS
hadd -k others.root		ttH.root ttW.root WW.root lowWW.root ZqqZll.root WqqZll.root llvvjj.root llvvjj_ss.root ttbar_2lep_PP8.root ttgamma.root singletop.root WWW.root ZZZ.root tWH.root tHjb.root tttt.root ttWW.root
####---- NOMINAL SAMPLES ----####

####---- ALTERNATIVE SAMPLES ----####
hadd -k PP8_ZZ.root		361603.root
hadd -k PP8_WZ.root		361601.root
####---- ALTERNATIVE SAMPLES ----####
