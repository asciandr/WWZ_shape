Setup environment & Run skimming
------
Setup ATLAS and root::

  . setup.sh

Run skimming tool locally, e.g.::

  nohup python run.py file_list/test.txt selection.txt outFolder.txt TTREE_NAME >& log &
  tail -f log

where, for instance, ``TTREE_NAME=nominal``.
You'll BETTER launch jobs on batch (1/TTree, here defined in ``ttrees_list.txt``, per each of ntuples given as input, here list in ``test.txt``, to be skimmed)::

  ./submit_batch.sh test.txt ttrees_list.txt
