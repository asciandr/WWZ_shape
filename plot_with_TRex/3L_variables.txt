#--- *branch name*		*units*	*bins*	*ledge*	*upedge*	*x-axis label* ---# 
# total_charge			;	;7	;-3.5	;3.5		;total lepton charge
# # # # ### @jet_pt.size()			;	;1	;0.5	;1.5		;event count
# # # # ### ### ### BDT3				;	;24	;-1.2	;1.2		;3\ell + 1\text{ jet BDT response}
# # # # ### ### ### BDT4				;	;15	;-1.2	;1.2		;3\ell + 2\text{ jets BDT response}
# # # # ### ### ### BDT5				;	;15	;-1.2	;1.2		;3\ell + \geq3\text{ jets BDT response}
# # # # ### ### ### ###### BDT0				;	;15	;-1.2	;1.2		;on-shell SF BDT response
# # # # ### ### ### ###### BDT1				;	;10	;-1.2	;1.2		;off-shell SF BDT response
# # # # ### ### ### ###### BDT2				;	;10	;-1.2	;1.2		;DF BDT response
# @jet_pt.size()			;	;7	;-0.5	;6.5		;number of jets
# @jet_pt.size()			;	;4	;0.5	;4.5		;number of jets
# average_mu			;	;35	;0.	;70.		;<\mu>
Mlll012/1000.			;GeV	;26	;80.	;600.		;m_{3\ell} \text{ [GeV]}
# Mll01/1000.			;GeV	;40	;0.	;600.		;m_{\ell\ell}^{01} \text{ [GeV]}
# Mll02/1000.			;GeV	;40	;0.	;600.		;m_{\ell\ell}^{02} \text{ [GeV]}
# Mll12/1000.			;GeV	;40	;0.	;600.		;m_{\ell\ell}^{12} \text{ [GeV]}
# # # # # Mll03/1000.			;GeV	;25	;0.	;300.		;m_{\ell\ell}^{03} \text{ [GeV]}
# # # # # Mll13/1000.			;GeV	;25	;0.	;300.		;m_{\ell\ell}^{13} \text{ [GeV]}
# # # # # Mll23/1000.			;GeV	;25	;0.	;300.		;m_{\ell\ell}^{23} \text{ [GeV]}
# # # # # # Mllll0123/1000.			;GeV	;40	;100.	;300.		;m_{4\ell} \text{ [GeV]}
# # # # # Mllll0123/1000.			;GeV	;22	;0.	;1100.		;m_{4\ell} \text{ [GeV]}
# (HT_lep+HT_had)/1000.		;GeV	;20	;0.	;2000.		;H_{\text{T}} \text{ [GeV]}
# HT_lep/1000.			;GeV	;15	;0.	;1000.		;H_{\text{T}}^{\text{lep}} \text{ [GeV]}
# HT_had/1000.			;GeV	;20	;0.	;2000.		;H_{\text{T}}^{\text{had}} \text{ [GeV]}
# best_Z_Mll/1000.		;GeV	;13	;71.2	;111.2		;M^{\text{best }Z}_{\ell\ell} \text{ [GeV]}
best_Z_Mll/1000.		;GeV	;7	;80.4	;102		;M^{\text{best }Z}_{\ell\ell} \text{ [GeV]}
# # # # best_Z_other_Mll/1000.		;GeV	;20	;0.	;200.		;M^{other best Z}_{\ell\ell} \text{ [GeV]}
# MET_RefFinal_et/1000.		;GeV	;30	;0.	;400.		;E_{\text{T}}^{\text{miss}} \text{ [GeV]}
# # # # # # # ### ### ###### MET/1000.			;GeV	;20	;0.	;200.		;uncalibrated MET \text{ [GeV]}
# # # # # # # ### ### ### best_Z_Mjj/1000.			;GeV    ;50   	;0.   	;500.          	;M^{best Z}_{jj} \text{ [GeV]}
# best_W_Mjj/1000.			;GeV    ;50   	;0.  	;500.          	;M^{\text{best }W}_{jj} \text{ [GeV]}
# # # # # # # ### ### ### dPhi_Wlv_Zll			;	;25	;-3.8	;3.8		;\Delta\phi(W\to\ell\nu,Z\to\ell\ell)
# # # # # # # ### ### ### dPhi_Wjj_Zll			;	;25	;-3.8	;3.8		;\Delta\phi(W\to jj,Z\to\ell\ell)
# # # # # # # ### ### ### dPhi_Wjj_Wlv			;       ;25     ;-3.8   ;3.8            ;\Delta\phi(W\to jj,W\to\ell\nu)
# # # # # # # ### ### ### dR_Wjj_Zll				;	;60	;-0.5	;5.5		;\Delta R(W\to jj,Z\to\ell\ell)
# Mjj01/1000.				;GeV	;50	;0.	;500.		;m_{jj}^{01} \text{ [GeV]}
# # # # # # # ### ### ### Mjj02/1000.				;GeV	;50	;0.	;500.		;m_{jj}^{02} \text{ [GeV]}
# # # # # # # ### ### ### Mjj12/1000.				;GeV	;50	;0.	;500.		;m_{jj}^{12} \text{ [GeV]}
# M_T_Wlv/1000.			;GeV 	;20   	;0. 	;150.   	;m_{\text{T}}^{W\to\ell\nu} \text{ [GeV]}
# # # # # # # ### ### ### pT_Zll/1000.			;GeV  	;30   	;0. 	;500.		;p_{T}^{Z\to\ell\ell} \text{ [GeV]}
# # # # # # # nJets_mv2c10_70			;	;5	;-0.5	;4.5		;n. of b-tagged jets (70%)
# # # # # # # nJets_mv2c10_77			;	;3	;-0.5	;2.5		;n. of b-tagged jets (77%)
# # nJets_mv2c10_85			;	;5	;-0.5	;4.5		;n. of b-tagged jets (85%)
# lepton_pt[0]/1000.		;GeV	;40	;0.	;400.		;\text{leading lepton }p_{T} \text{ [GeV]}
# lepton_pt[1]/1000.		;GeV	;40	;0.	;400.		;\text{subleading lepton }p_{T} \text{ [GeV]}
# lepton_pt[2]/1000.		;GeV	;40	;0.	;400.		;3^{rd}\text{ leading lepton }p_{T} \text{ [GeV]}
# # # # # # # ### lepton_pt[3]/1000.		;GeV	;20	;0.	;200.		;4^{th} leading lepton p_{T} \text{ [GeV]}
# jet_pt[0]/1000.			;GeV	;60	;0.	;1000.		;\text{leading jet }p_{T} \text{ [GeV]}
# jet_pt[1]/1000.			;GeV	;48	;0.	;800.		;\text{subleading jet }p_{T} \text{ [GeV]}
# # # jet_pt[2]/1000.			;GeV	;24	;0.	;400.		;3^{rd} leading jet p_{T} \text{ [GeV]}
# jet_eta[0]			;	;20	;-4.	;4.		;\text{leading jet }\eta
jet_eta[0]			;	;12	;-2.4	;2.4		;\text{leading jet }\eta
# # # # # # # ### ### jet_tot_invMass/1000.		;GeV   	;15   	;0.    	;1500.         	;m_{all jets} \text{ [GeV]}
# # # # # # # ### ### dPhi_WZ3l1v_1jet			;	;25   	;-4.  	;4.           	;\Delta\phi(WZ\to3\ell1\nu,jet)
# # # # # # # ### ### notWjjCand_leadJet_pt/1000.		;GeV   	;100   	;0.   	;500.       	;not W \to jj lead jet p_{T} \text{ [GeV]}
# # # # # # # ### ### ## notWjjCand_leadJet_eta		;       ;30     ;-4.    ;4.             ;not W \to jj lead jet \eta
# smallest_Mjj/1000.			;GeV	;25	;0.	;250.		;\text{smallest }m_{jj} \text{ [GeV]}
# leptons_jets_MET_tot_invMass/1000.	;GeV	;50	;0.	;2000.		;m_{\text{leptons, jets, }E_{\text{T}}^{\text{miss}}} \text{ [GeV]}
# Zll_leadJet_invMass/1000.		;GeV	;20	;0.	;500.		;m_{\text{leading jet, }Z\to\ell\ell}
# # # # # # # ### ### dR_Zll_other_l			;	;30   	;-0.5 	;5.5        	;\Delta R(Z\to\ell_0\ell_1,\ell_2)
# # # # # # # ### ### Zll_leadJet_dR			;	;30   	;-0.5 	;5.5        	;\Delta R(leading jet, Z\to\ell\ell,)
# # # # # # # ### ### ### Mjj_central_fwd/1000.		;GeV	;20	;0.	;300.		;m_{central jet , forward jet (\geq 30 GeV)}
