#!/bin/bash

PACKAGE_folder=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/plot_with_TRex/
TREX_folder=/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/

ls -haltr ${PACKAGE_folder}/CONFIG

cp -rf ${TREX_folder}/*.C 					.
cp -rf ${TREX_folder}/*.h 					.
cp -rf ${TREX_folder}/*.sh 					.
cp -rf ${TREX_folder}/*.py 					.
cp -rf ${TREX_folder}/TtHFitter/ 				.
cp -rf ${TREX_folder}/Root/					.
cp -rf ${TREX_folder}/util/ 					.
cp -rf ${TREX_folder}/Makefile 					.

cp -rf ${PACKAGE_folder}/CONFIG          			.
cp -rf ${TREX_folder}/my_Common_XS_unc.txt			.

ls -haltr
. setup.sh
make
./myFit.exe nd CONFIG
echo 'Plotting done!'
mkdir -p ${TREX_folder}/JOB_NAME/
cp -r JOB_NAME/Plots ${TREX_folder}/JOB_NAME/
cp -r JOB_NAME/Tables ${TREX_folder}/JOB_NAME/
echo 'I AM DONE.'
