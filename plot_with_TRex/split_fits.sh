#!/bin/bash

listtxt=$*

JOBLINE=`cat $listtxt | grep Job: `
JOBNAME=`echo "$JOBLINE" | cut -d' ' -f 2`

echo "JOBLINE: $JOBLINE"
echo "JOBNAME: $JOBNAME"

cat job.sh | sed "s/JOB_NAME/$JOBNAME/g" | sed "s/CONFIG/$listtxt/g" > job_$listtxt.sh
chmod 755 job_$listtxt.sh
queue="8nh"
#queue="1nd"
#queue="2nd"
bsub -q $queue job_$listtxt.sh
